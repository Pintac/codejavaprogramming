package section_9.interfaces;

public class DeskPhone implements ITelephone {
    private int myNumber;
    private boolean isRinging;

    public DeskPhone(int myNumber) {
        this.myNumber = myNumber;
    }

    @Override
    public void powerOn() {
        System.out.println("No Action taken, desk phone does not have a power button");
    }

    @Override
    public void dial(int phoneNumber) {
        System.out.println("Now ringing " + phoneNumber + " on deskphone");
        isRinging = true;
    }

    @Override
    public void answer() {
        if (isRinging) {
            System.out.println("Answering the deskphone");
            isRinging = false;
        }else {
            System.out.println("No one is calling");
        }
    }

    @Override
    public boolean callPhone(int phoneNumber) {
        if(phoneNumber ==myNumber){
            isRinging = true;
            System.out.println("Ring ring");
        }else {
            System.out.println("Wrong number");
            isRinging = false;
        }
        return isRinging;
    }

    @Override
    public boolean isRinging() {
        System.out.println("Ringing? : "+isRinging);
        return isRinging;
    }
}