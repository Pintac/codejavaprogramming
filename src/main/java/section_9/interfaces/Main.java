package section_9.interfaces;

public class Main {
    public static void main(String[] args) {
        ITelephone timsPhone = new DeskPhone(123456);

        timsPhone.powerOn();
        timsPhone.callPhone(1236456);
        timsPhone.dial(123456);
        timsPhone.answer();
        timsPhone.isRinging();

    }
}
