package section_x.zoom;

import javax.swing.*;
import java.awt.*;

public class Zoom {
    public JFrame frame;
    public JButton serviceMeetingButton, tagalogMeetingButton;
    public int width = 150;
    public int height = 30;
    public Link link = new Link();
    JLabel label, footerLabel;

    public Zoom() {
        ImageIcon icon = new ImageIcon("C:\\Users\\loyd_\\Desktop\\this.jpeg");
        ImageIcon img = new ImageIcon("C:\\Users\\loyd_\\Desktop\\Logo.gif");
        Font font = new Font("Serif", Font.BOLD + Font.ITALIC, 8);

        frame = new JFrame("Zoom meeting");
        frame.setLayout(null);
        frame.setSize(490, 280);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);

        label = new JLabel();
        label.setIcon(img);
        label.setBounds(1, 1, 474, 230);

        footerLabel = new JLabel("© Copyright 2020. All Rights Reserved |  Powered By: LLOYDEE TECHNOLOGY ");
        footerLabel.setBounds(2, 232, 400, 10);
        footerLabel.setForeground(Color.BLUE);
        footerLabel.setFont(font);
        //footerLabel.setIcon(icon);

        serviceMeetingButton = new JButton("Service Meeting");
        serviceMeetingButton.setForeground(Color.BLUE);
        serviceMeetingButton.setBounds(170, 50, width, height);
        serviceMeetingButton.addActionListener(e -> link.opeServiceMeetingLink());

        tagalogMeetingButton = new JButton("Tagalog Meeting");
        tagalogMeetingButton.setForeground(Color.BLUE);
        tagalogMeetingButton.setBounds(170, 140, width, height);
        tagalogMeetingButton.addActionListener((e -> link.openMeetingLink()));

        frame.add(serviceMeetingButton);
        frame.add(tagalogMeetingButton);
        frame.add(label);
        frame.add(footerLabel);

        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new Zoom();
    }
}
