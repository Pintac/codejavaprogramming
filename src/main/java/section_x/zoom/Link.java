package section_x.zoom;

import org.testng.annotations.Test;
import section_x.framework.Methods;

public class Link {
    public String serviceURL = "https://us02web.zoom.us/j/84836392181?pwd=VElYUzAzYUVJT01HM203QnZtOFBKdz09";
    public String meetingURL = "https://us02web.zoom.us/j/81794059449?pwd=YjlEOXFFS2FDNHR3Z3VFRVoxc0g4Zz09";


    Methods methods = new Methods();

    @Test
    public void openMeetingLink() {
        methods.getDriver(meetingURL);

    }

    @Test
    public void opeServiceMeetingLink() {
        methods.getDriver(serviceURL);
    }
}
