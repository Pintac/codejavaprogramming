package section_x.framework;

import org.openqa.selenium.By;

public class Web {

    public static final By JWSTREAMUSERNAME =By.id("username")  ;
    public static final By JWSTREAMPASSWORD =By.id("password")  ;

    public By SEARCHMANGA = By.linkText("One Piece");
    public By SEARCHANIME = By.id("keyword");
    public By ONEPIECE = By.linkText("One Piece (Sub)");
    public By MENUVIDEO = By.id("my_video_1_html5_api");
    public By ONEPIECEEPISODE = By.linkText("One Piece (Sub) Episode 929");

    public static final By YOURLETTERCLICKSEARCH = By.xpath("//*[@id=\"new_word_search\"]/div[2]/div[2]/button/i");
    public String ultimatixURL = "https://www.ultimatix.net/";
    public String whatsAppURL = "http://web.whatsapp.com";
    public String googleURL = "https://www.google.com/";

    public static final By YOURLETTERSSEARCH = By.id("word_search_letters");

    //watchtower study
    //public By MEETINGS = By.linkText("MEETINGS");
    public By MEETINGS = By.xpath("//*[@id=\"menuToday\"]/a/span[2]");
    public By LIBRARY = By.linkText("LIBRARY");
    public By ONLINELIBRARY = By.linkText("Online Library");
    public By WATCHTOWER = By.xpath("//*[@id=\"p9\"]/a/strong");
    public By PLAYCURRENTWATCHTOWER = By.id("playSelectedButton");
    //midweek meeting
    public By MEETINGWORKBOOK = By.xpath("//*[@id=\"article\"]/article/div[2]/div[2]/div[1]/div/a");

    public By USERNAME = By.id("form1");
    public By PROCEEDBUTTON = By.id("proceed-button");
    public By PASSBUTTON = By.id("password-btn");
    public By PASSWORD = By.id("password-login");
    public By SUBMITBUTTON = By.id("form-submit");
    public By BIBLETEACHINGS = By.linkText("BIBLE TEACHINGS");
    public By LYRICSVIEW = By.xpath("//*[@id=\"article\"]/article/div[4]/div/div/div[1]/div/ul/li[1]/button/span[2]/svg");
    public By SEARCHJW = By.name("q");
    public By VIDEO = By.xpath("//*[@id=\"article\"]/article/div[2]/form/div[2]/div/button[3]");
    public By VIDEOBREAD = By.linkText("How to Make Memorial Bread");
    public By VIDEOLORDSEVENINGMEAL = By.linkText("19. The Lord's Evening Meal");
    public By VIDEOOURJOYETENALLY = By.linkText("Our Joy Eternally");
    public By VIDEOMEMORIALTALK = By.linkText("2020 Memorial Talk—United States");
    public By VIDEOMAKEJEHOVAHHAPPY = By.linkText("Make Jehovah Happy");
    public By VIDEOTALKBYDAVIDSPLANEL = By.linkText("David H. Splane: Inviting All to the Memorial (Ps. 118:22)");
    public By VIDEOMORNINGWORSHIP = By.linkText("David H. Splane: Inviting All to the Memorial (Ps. 118:22)");
    public By VIDEOTALKBYMARKNOURMAIR = By.linkText("Mark Noumair: He Used His Body to Honor Jehovah (1 Cor. 11:24)");
    public By PLAYVIDEO = By.xpath("//*[@id=\"vjs_video_3\"]/button");
    public By PLAYVIDEO1 = By.xpath("//*[@id=\"vjs_video_3\"]/div[4]/div[1]/button[1]");
    public By PLAY = By.xpath("//*[@id=\"article\"]/article/div[4]/div/div/div[1]/div/ul/li[3]/button/span[2]/svg");

    public By MAXVID = By.xpath("//*[@id=\"vjs_video_3\"]/div[4]/div[4]/button[2]/span[1]");
    public By LANGUAGEDD = By.xpath("//*[@id=\"otherAvailLangs\"]/form/div/input");

    public By GOOGLESEARCH = By.name("q");
    public By GOOGLESEARCHBUTTON = By.name("btnK");

    public String sourceFile = "C:\\Users\\loyd_\\Desktop\\timesheet.xlsx";

    public String elementDescription = "Description";
    public String language = "Tagalog";
    public byte time = 1;
    public byte waitTime = 30;
    public String wordTips = "https://word.tips/";
    public By searchRecipientName = By.xpath("//*[@id=\"side\"]/div[1]/div/label/div/div[2]");
    public By enterMessage = By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]");
    public By sendMessage = By.xpath("//*[@id=\"main\"]/footer/div[1]/div[3]");
    public By weekBibleReading = By.xpath("//*[@id=\"p2\"]/a/strong");
    public By playBibleReading = By.xpath("//*[@id=\"mep_0\"]/div/div[3]/div[1]/button");

    //chess.com
    public By SIGNUP = By.linkText("Sign Up");
    public By USERNAMECHESS = By.id("registration_username");
    public By EMAILCHESS = By.id("registration_email");
    public By PASSWORDCHESS = By.id("registration_password");
    public By REGISTERCHESS = By.id("registration_submit");
}
