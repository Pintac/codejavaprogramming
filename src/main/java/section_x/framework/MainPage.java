package section_x.framework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

public class MainPage {
    Methods methods = new Methods();
    Web web = new Web();

    public void searchGoogle(String search) throws Exception {
        methods.getDriver(web.googleURL);
        methods.sendKeysAndEnter(web.GOOGLESEARCH, search);
        methods.toClick(web.GOOGLESEARCHBUTTON, web.elementDescription, web.time);
    }

    public void searchWords(String letters) throws Exception {
        methods.getDriver(web.wordTips);
        methods.sendKeys(web.YOURLETTERSSEARCH, letters);
        methods.toClick(web.YOURLETTERCLICKSEARCH, web.elementDescription, web.time);
    }

    public void logInFlow() throws Exception {
        methods.getDriver(web.ultimatixURL);
        openTimeSheet();
        openTimesheetLink();
    }

    public void openTimeSheet() throws Exception {
        methods.sendKeys(web.USERNAME, methods.getData(0, 1, web.sourceFile));
        studyWatchtower(web.PROCEEDBUTTON, web.PASSBUTTON);
        enterPassword();
    }

    public void playMemorialVideoFlow() throws Exception {
        openTheJW();
        methods.sendKeysAndEnter(web.SEARCHJW, methods.getData(4, 1, web.sourceFile));
        studyWatchtower(web.VIDEO, web.VIDEOBREAD);
        methods.toClick(web.PLAYVIDEO, web.elementDescription, web.time);
        methods.toClick(web.MAXVID);
        //methods.closeDriver();
    }

    public void playSongLordsEveningVideoFlow() throws Exception {
        openTheJW();
        methods.sendKeysAndEnter(web.SEARCHJW, methods.getData(4, 1, web.sourceFile));
        studyWatchtower(web.VIDEO, web.VIDEOLORDSEVENINGMEAL);
        // selectLanguage();
        //methods.click(web.PLAY, web.elementDescription, web.time);
        methods.toClick(web.PLAYVIDEO, web.elementDescription, web.time);
        //methods.click(web.MAXVID, web.elementDescription, web.time);
    }

    public void playSongOurJoyEternally() throws Exception {
        openTheJW();
        methods.sendKeysAndEnter(web.SEARCHJW, methods.getData(7, 1, web.sourceFile));
        //methods.toClick(web.VIDEO, web.elementDescription, web.time);
        studyWatchtower(web.VIDEOOURJOYETENALLY, web.PLAYVIDEO1);
    }

    public void regression() throws Exception {
        openTheJW();

    }

    private void selectLanguage() {
        methods.toClick(web.LANGUAGEDD, web.elementDescription, web.time);
        methods.sendKeysAndEnter(web.LANGUAGEDD, web.language);
    }

    public void playMemoriaTalkUSA() throws Exception {
        openTheJW();
        methods.sendKeysAndEnter(web.SEARCHJW, methods.getData(4, 1, web.sourceFile));
        studyWatchtower(web.VIDEO, web.VIDEOMEMORIALTALK);
        methods.toClick(web.PLAYVIDEO, web.elementDescription, web.time);

    }

    public void enterPassword() throws Exception {
        methods.waitTime();
        methods.sendKeys(web.PASSWORD, methods.getData(1, 1, web.sourceFile));
        methods.toClick(web.SUBMITBUTTON, web.elementDescription, web.time);

    }

    public void openTimesheetLink() {
        WebDriverWait wait = new WebDriverWait(methods.driver, web.time);
        WebElement timesheetEntryLink = wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Timesheet Entry")));
        timesheetEntryLink.click();
    }

    public void readThisWeeksWatchtowerStudy() throws Exception {
        openTheJW();
        goToWOL();

    }

    public void openTheJW() throws Exception {
        methods.getDriver(methods.getData(3, 1, web.sourceFile));
    }


    public void goToWOL() {
        studyWatchtower(web.LIBRARY, web.ONLINELIBRARY);
        methods.moveToOtherTabs();
        studyWatchtower(web.MEETINGS, web.PLAYCURRENTWATCHTOWER);
    }

    public void studyWatchtower(By watchtower, By playcurrentwatchtower) {
        methods.toClick(watchtower, web.elementDescription, web.time);
        methods.toClick(playcurrentwatchtower, web.elementDescription, web.time);
    }

    public void readThisWeeksBibleReading() throws Exception {
        openTheJW();
        goToWOL();
        readBibleReading(web.weekBibleReading, web.playBibleReading);
    }


    public void watchAnime(String title) throws Exception {
        methods.getDriver(methods.getData(5, 1, web.sourceFile));
        methods.sendKeysAndEnter(web.SEARCHANIME, title);
        methods.toClick(web.ONEPIECE, web.elementDescription, web.time);
        methods.moveToOtherTabs(0);
        methods.toClick(web.ONEPIECEEPISODE, web.elementDescription, web.time);
        methods.toRightClick(web.MENUVIDEO);
        methods.saveToLocal();
    }

    public void readManga(String title) throws Exception {
        methods.getDriver(methods.getData(6, 1, web.sourceFile));
        methods.sendKeysAndEnter(web.SEARCHANIME, title);
        methods.toClick(web.SEARCHMANGA, web.elementDescription, web.time);

    }

    public void openWhatsApp(String name, String text, int number) {
        methods.getDriver(web.whatsAppURL);
        methods.toClick(web.searchRecipientName, web.elementDescription, web.time);
        methods.sendKeysAndEnter(web.searchRecipientName, name);
        for (int x = 0; x < number; x++) {
            methods.sendKeys(web.enterMessage, text);
            methods.click(web.sendMessage);

        }

    }

    public void downloadJWStreamingTalk() throws Exception {
        methods.getDriver(methods.getData(8, 1, web.sourceFile));
        logIn();
    }

    private void logIn() throws Exception {
        methods.sendKeys(web.JWSTREAMUSERNAME, methods.getData(8, 1, web.sourceFile));
        methods.clickTab(web.JWSTREAMUSERNAME);
        //methods.sendKeys(web.JWSTREAMPASSWORD,methods.getData(9, 1, web.sourceFile));
    }

    private void readBibleReading(By weekBibleReading, By playBibleReading) {
        methods.toClick(weekBibleReading, web.elementDescription, web.time);
        methods.toClick(playBibleReading, web.elementDescription, web.time);
    }

    public void signUpFlow() throws IOException {
        methods.getDriver("https://www.chess.com");
        methods.toClick(web.SIGNUP, web.elementDescription, web.time);
        String x = methods.sendKeys(web.USERNAMECHESS, methods.getRandom()+"xx"+methods.getRandom());
        String y = methods.sendKeys(web.EMAILCHESS, "Hi" + methods.getRandom()+"@yahoo.com");
        methods.sendKeys(web.PASSWORDCHESS, "SimilarPass123");
        methods.write("C:\\Users\\loyd_\\Desktop\\X.txt","\nUsername: "+x+"\t Password: SimilarPass123 "+" \tEmail: "+y+"\n");
        methods.toClick(web.REGISTERCHESS, web.elementDescription, web.time);
    }

}
