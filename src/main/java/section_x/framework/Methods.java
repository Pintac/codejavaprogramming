package section_x.framework;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Methods {
    public WebDriver driver;
    private Random random = new Random();
    public void write(String path, String text) throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter(path, true));
        writer.write(text);
        System.out.println("Done!! " + text + " has been updated to " + path + " file");
        writer.close();
    }

    public void getDriver(String URL) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get(URL);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    }

    public void tab(By by) {
        for (int x = 0; x <= 8; x++) {
            driver.findElement(by).sendKeys(Keys.TAB);
            highlightElement(by);
        }
    }

    public int getRandom() {
        int randomNumber = random.nextInt(1000);
        return randomNumber;
    }

    public void toClick(By by, String elementDescription, int time) {
        //waitForPageLoading();
        highlightElement(by);
        scrollDown(by);
        checkIfDisplayed(by, elementDescription, time);
        driver.findElement(by).click();
    }

    public void selectDropDwon(By by, String item) {
        Select obj = new Select(driver.findElement(by));
        obj.selectByVisibleText(item);
    }

    public void toClick(By by) {
        waitForPageLoading();
        driver.findElement(by).click();
    }

    private void hover(By by) {
        Actions action = new Actions(driver);
        WebElement we = driver.findElement(by);
        action.moveToElement(we).build().perform();
    }

    public void closeDriver() {
        driver.close();
    }

    private void highlightElement(By by) {
        WebElement element = driver.findElement(by);
        {
            try {
                JavascriptExecutor js = (JavascriptExecutor) driver;
                for (int i = 0; i < 3; i++) {
                    js.executeScript("arguments[0].style.border='3px groove blue'", element);
                    Thread.sleep(1000); // wait for see blinking
                    js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid blue;');", element);
                    js.executeScript("arguments[0].style.border=''", element);
                }
            } catch (Exception E) {
                System.out.println("Error in Highlight element");
            }
        }
    }

    private void scrollDown(By by) {
        WebElement element = driver.findElement(by);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
    }

    public String sendKeys(By by, String text) {
        driver.findElement(by).sendKeys(text);
        return text;
    }

    public void sendKeysAndEnter(By by, String text) {
        highlightElement(by);
        WebElement element = driver.findElement(by);
        element.sendKeys(text);
        element.sendKeys(Keys.RETURN);

    }

    public void saveToLocal() {
        Actions actions = new Actions(driver);
        actions.sendKeys(Keys.CONTROL + "s").perform();
    }

    public void toRightClick(By by) {
        Actions actions = new Actions(driver);
        WebElement element = driver.findElement(by);
        actions.contextClick(element).perform();
    }

    public void checkIfDisplayed(By by, String elementDescription, int time) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, time);
            wait.until(ExpectedConditions.presenceOfElementLocated(by));
        } catch (Exception e) {
            System.out.println(elementDescription + " not displayed");
        }
        waitForPageLoading();
    }

    public void waitForPageLoading() {
    }

    public void waitTime() {
    }

    public String getData(int row, int column, String sourceFile) throws Exception {

        FileInputStream file = new FileInputStream(new File(sourceFile));
        // Create Workbook instance holding reference to .xlsx file
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        // Get first/desired sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(0);
        // Iterate through each rows one by one
        DataFormatter formatter = new DataFormatter();
        String data = formatter.formatCellValue(sheet.getRow(row).getCell(column));
        file.close();
        return data;
    }

    public void moveToOtherTabs() {
        ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
        System.out.println(tabs2.size());
        driver.switchTo().window(tabs2.get(1));
    }

    public void moveToOtherTabs(int tab) {
        ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
        if (tab == 1) {
            driver.switchTo().window(tabs2.get(0));
            driver.close();
            driver.switchTo().window(tabs2.get(tab));

        } else if (tab == 0) {
            driver.switchTo().window(tabs2.get(1));
            driver.close();
            driver.switchTo().window(tabs2.get(tab));
        }

    }

    public void click(By searchRecipientName) {
        driver.findElement(searchRecipientName).click();
    }

    public void clickTab(By by) {
        WebElement element = driver.findElement(by);
        element.sendKeys(Keys.TAB);
    }
}


