package section_x.tests;

import org.testng.annotations.Test;
import section_x.framework.MainPage;

public class MeetingStudy {
    public static  MainPage mainPage = new MainPage();


    @Test(priority = 0)
    public void studyWatchtower() throws Exception {
        mainPage.readThisWeeksWatchtowerStudy();
    }

   // @Test()
    public void studyMidweekMeeting() throws Exception {
        mainPage.readThisWeeksBibleReading();
    }
}
