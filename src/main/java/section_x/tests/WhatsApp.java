package section_x.tests;

import org.testng.annotations.Test;
import section_x.framework.MainPage;


public class WhatsApp {
    MainPage mainPage = new MainPage();

    @Test()
    public void sendMessage() throws Exception {
        mainPage.openWhatsApp("Edith Pintac", "Automated message",1);
    }
}
