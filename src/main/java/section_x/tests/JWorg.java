package section_x.tests;

import org.testng.annotations.Test;
import section_x.framework.MainPage;


public class JWorg {
    MainPage mainPage = new MainPage();
    @Test(priority = 0)
    public void playVideos0() throws Exception {
        mainPage.playMemorialVideoFlow();
    }
    //@Test(priority = 1)
    public void playVideos1() throws Exception {
        mainPage.playSongLordsEveningVideoFlow();
    }

    // @Test(priority = 2)
    public void playVideos2() throws Exception {
        mainPage.playMemoriaTalkUSA();
    }
    //@Test(priority = 3)
    public void regression() throws Exception {
        mainPage.regression();
    }
    @Test(priority = 1)
    public void ourJoySong() throws Exception {
        mainPage.playSongOurJoyEternally();
    }
}
