package section_x.tests;

import org.testng.annotations.Test;
import section_x.framework.MainPage;

public class Google {
    MainPage mainPage = new MainPage();

    @Test()
    public void openGoogle() throws Exception {
        mainPage.searchGoogle("What to buy for anniversary");
    }

}
