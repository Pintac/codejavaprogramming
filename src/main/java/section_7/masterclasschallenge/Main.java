package section_7.masterclasschallenge;

class Hamburger {
    private String name;
    private String breadType;
    private String meat;
    private double price;

    private double lettuce;
    private double tomato;
    private double cheese;
    private double carrots;

    public Hamburger(String name, String breadType, String meat, double price) {
        this.name = name;
        this.breadType = breadType;
        this.meat = meat;
        this.price = price;
        this.lettuce = .75;
        this.tomato = .50;
        this.cheese = 1.00;
        this.carrots = 0.80;
    }

    public double getLettuce() {
        System.out.println("Adding Lettuce with a price of " + this.lettuce);
        return this.price += lettuce;
    }

    public double getTomato() {
        System.out.println("Adding Tomato with a price of " + this.tomato);
        return this.price += tomato;
    }

    public double getCheese() {
        System.out.println("Adding Cheese with a price of " + this.cheese);
        return this.price += cheese;
    }

    public double getCarrots() {
        System.out.println("Adding Carrots with a price of " + this.carrots);
        return this.price += carrots;
    }

    public double getTotalPrice() {
        double hamburgerPrice = this.price;
        System.out.println(this.name + " hamburger " + "on a " + this.breadType + " roll with " + this.meat + ", price is " + this.price);
        return hamburgerPrice;
    }
}

class HealthyBurger extends Hamburger {
    private double mushroom;
    private double cucumber;
    private double price;

    public HealthyBurger(String meat) {
        super("Healthy", "Brown Rye", meat, 6);
        this.mushroom = 1.50;
        this.cucumber = 2.75;
        this.price = 6;
    }

    public double getMushroom() {
        System.out.println("Adding Mushcroom with a price of " + this.mushroom);
        return this.price += mushroom;
    }

    public double getCucumber() {
        System.out.println("Adding Cucumber with a price of " + this.cucumber);
        return this.price += cucumber;
    }

    @Override
    public double getTotalPrice() {
        super.getTotalPrice();
        return this.price;
    }
}


class DeluxeBurger extends Hamburger {
    private String chips;
    private String drinks;

    public DeluxeBurger() {
        super("Deluxe", "White", "Sausage & Bacon", 10.50);
        this.chips = "Potato Chips";
        this.drinks = "Coke";
    }

    public DeluxeBurger(String name, String breadType, String meat, double price) {
        super(name, breadType, meat, price);
    }

    @Override
    public double getLettuce() {
        System.out.println("Cannot add additional item to deluxe burger");
        return 0;
    }

    @Override
    public double getTomato() {
        System.out.println("Cannot add additional item to deluxe burger");
        return 0;
    }

    @Override
    public double getCheese() {
        System.out.println("Cannot add additional item to deluxe burger");
        return 0;
    }

    @Override
    public double getCarrots() {
        System.out.println("Cannot add additional item to deluxe burger");
        return 0;
    }

    public String getChips() {
        System.out.println("Adding chips");
        return chips;
    }

    public String getDrinks() {
        System.out.println("Adding drinks");
        return drinks;
    }
}

public class Main {
    public static void main(String[] args) {
        Hamburger hamburger = new Hamburger("Cheese Burger", "Wheat", "Bacon", 4.99);
        hamburger.getTotalPrice();
        hamburger.getLettuce();
        hamburger.getTomato();
        hamburger.getCheese();
        System.out.println("Total Burger price is " + hamburger.getTotalPrice() + "\n");

        HealthyBurger healthyBurger = new HealthyBurger("Sausage");
        healthyBurger.getTotalPrice();
        healthyBurger.getCucumber();
        healthyBurger.getMushroom();
        System.out.println("Total price is " + healthyBurger.getTotalPrice() + "\n");

        DeluxeBurger deluxeBurger = new DeluxeBurger();
        deluxeBurger.getChips();
        deluxeBurger.getTotalPrice();
        deluxeBurger.getDrinks();
    }
}