package section_7.composition.challengetime;

public class Sofa {
    private String color;
    private int seats;

    public Sofa(String color, int seats) {
        this.color = color;
        this.seats = seats;
    }

    public String getColor() {
        System.out.println("The color of your sofa is "+color);
        return color;
    }

    public int getSeats() {
        return seats;
    }
}
