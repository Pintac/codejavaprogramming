package section_7.composition.challengetime;

public class Main {
    public static void main(String[] args) {
        TV tv = new TV(42,24);
        Sofa sofa = new Sofa("Blue",3);
        Table table = new Table(23,53,10,4);

        Room room = new Room(tv,sofa,table  );
        room.getTable();
        room.getSofa();
        room.getTv(tv.getWidth(),tv.getWidth());
        System.out.println(tv.getWidth());
    }
}
