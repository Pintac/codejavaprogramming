package section_7.composition.challengetime;

public class Table {
    private int length;
    private int width;
    private int height;
    private int legs;

    public Table(int length, int width, int height, int legs) {
        this.length = length;
        this.width = width;
        this.height = height;
        this.legs = legs;
    }

    public void getTable(int length, int width, int height, int legs) {
        System.out.println("The Table has "+legs+" with "+height+" tall and "+length*width+" area");
    }

    private int getLength() {
        return length;
    }

    private int getWidth() {
        return width;
    }

    private int getHeight() {
        return height;
    }

    private int getLegs() {
        return legs;
    }
}
