package section_7.composition.challengetime;

public class TV {
    private int width;
    private int length;

    public TV(int width, int length) {
        this.width = width;
        this.length = length;
    }

    public int getTVSize(int width, int length){
        System.out.println(width*length+" is the size of the tv");
        return width*length;
    }
    
    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

}
