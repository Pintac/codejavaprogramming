package section_7.composition.challengetime;

public class Room {
    private TV tv;
    private Sofa sofa;
    private Table table;

    public Room(TV tv, Sofa sofa, Table table) {
        this.tv = tv;
        this.sofa = sofa;
        this.table = table;
    }

    public TV getTv(int length, int width) {
        System.out.println("Getting the tv");
        tv.getTVSize(length,width);
        return tv;
    }

    public Sofa getSofa() {
        System.out.println("Getting the sofa");
        sofa.getColor();
        return sofa;
    }

    public Table getTable() {
        System.out.println("Getting the table");
        table.getTable(12,23,25,4);
        return table;
    }
}
