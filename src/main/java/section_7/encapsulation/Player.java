package section_7.encapsulation;

public class Player {

    private String fullName;
    private int attackPower;
    private int health = 200;
    private String weapon;

    public Player(String fullName, int health, String weapon, int attackPower) {
        this.fullName = fullName;
        this.attackPower = attackPower;
        this.weapon = weapon;
        if (health > this.health && health <= 500) {
            this.health = health;
        }
    }

    public void loseHealth(int damage) {
        this.health = this.health - damage;
        if (this.health <= 0) {
            System.out.println("Player knocked out");
        }
    }

    public void attack(String player, int power, String opponent, int health) {
        this.health = health - power;
        System.out.println(player + " attacked " + opponent + " with " + weapon + ", " + opponent + "'s health is now " + this.health);
        if(this.health <=0){
            System.out.println(opponent +" lose this fight");
        }
    }

    public void getStats() {
        System.out.println("Player " + this.fullName + " has " + this.health + " health remaining");
    }

    public int healthRemaining() {
        return this.health;
    }

    public String getFullName() {
        return fullName;
    }

    public int getHealth() {
        return health;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }
}
