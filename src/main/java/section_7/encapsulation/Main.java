package section_7.encapsulation;

public class Main  {

    public static void main(String[] args) {
        EnhancedPlayer player = new EnhancedPlayer("Time",200,"Sword");
        System.out.println("Initial health is "+player.getHitPoints());

        Player player1 = new Player("Lass", 200, "Dagger", 400);
        Player player2 = new Player("Ronan", 350, "Spell Sword", 150);
        Player player3 = new Player("Ryan", 401, "Axe", 130);

        fight(player1, player3);
    }

    public static void fight(Player player1, Player player2) {
        player1.attack(player1.getFullName(), player1.getAttackPower(), player2.getFullName(), player2.getHealth());
    }


}
