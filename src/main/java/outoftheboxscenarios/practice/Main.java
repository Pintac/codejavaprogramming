package outoftheboxscenarios.practice;

import java.util.Scanner;

public class Main {
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final int CONST = 10;

    public static void main(String[] args) {

        reverseWordByStringBuffer("3231645");
        //getInput();
    }

    private static void getInput() {
        System.out.println("Select an option below:");
        System.out.println("N for numbers\nS for String");
        String prompt = SCANNER.nextLine().toUpperCase();
        switch (prompt) {
            case "N":
                reverseNumber(getNumbers());
                break;
            case "S":
                reverseWord(getWords());
                break;
            default:
                System.out.println("Wrong Input");
        }
    }


    private static void reverseWordByStringBuffer(String getWords) {
        StringBuffer stringbuffer = new StringBuffer();
        System.out.print("Reverse of " + getWords + " is " + stringbuffer.append(getWords).reverse());
    }

    private static String reverseWord(String words) {
        String reverseWord = null;
        for (int x = words.length() - 1; x >= 0; x--) {
            reverseWord = words.substring(x, x + 1);
            System.out.print(reverseWord.toUpperCase());
        }
        return reverseWord;
    }

    private static String getWords() {
        System.out.println("Enter any words\r");
        return SCANNER.nextLine();

    }

    private static int reverseNumber(int number) {
        int reverseNumber = 0;
        if (number > 0) {
            System.out.print("The reverse of number " + number + " is = ");
            while (number > 0) {
                reverseNumber = number % CONST;
                System.out.print(reverseNumber);
                number /= CONST;
            }
        } else {
            System.out.println(number + " is invalid input, please enter positive number only!!");
        }

        return reverseNumber;
    }

    private static int getNumbers() {
        int number = 0;
        System.out.println("Enter number to reverse\r");
        try {
            number = Integer.parseInt(SCANNER.nextLine());

        } catch (NumberFormatException x) {
            System.out.println("Invalid Input");
        }
        return number;
    }
}
