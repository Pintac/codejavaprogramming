package outoftheboxscenarios.practice;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Sample {
    public static ArrayList<String> cats = new ArrayList<>();
    public static Scanner scanner = new Scanner(System.in);
    public static String DBFILE = "C:\\\\Users\\\\loyd_\\\\Desktop\\\\catDatabase.txt";

    public static void main(String[] args) throws IOException {


        updateDB(DBFILE);

        instruction();
        boolean input = true;
        while (input) {
            System.out.println("Press '6' for options...");
            int option = scanner.nextInt();
            scanner.nextLine();
            switch (option) {
                case 0:
                    printName();
                    break;
                case 1:
                    addName();
                    break;
                case 2:
                    deleteName();
                    break;
                case 3:
                    modifyName();
                    break;
                case 4:
                    searchName();
                    break;
                case 5:
                    capitalizeName();
                    break;
                case 6:
                    instruction();
                    break;
                case 7:
                    pullData();
                    break;
                case 8:
                    pushData();
                    break;
                case 9:
                    pushData(DBFILE);
                    input = false;
                    break;
                default:
                    System.out.println("Wrong options");
            }
        }
    }

    public static void updateDB(String s) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(s));
        String names;
        while ((names = reader.readLine()) != null) {
            if (!onFile(names)) {
                cats.add(names);
            }
        }
    }

    public static void pullData() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\loyd_\\Desktop\\sampl.txt"));
        String names;
        while ((names = reader.readLine()) != null) {
            if (!onFile(names)&&(cats.size()<100)) {
                cats.add(names);
            }else {
                System.out.println("Max Limit OR name/s already exist!");
                return;
            }
        }
        System.out.println("data imported...");
    }


    private static void pushData(String location) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(location));
        if (hasData()) return;
        for (String cat : cats) {
            writer.write(cat + "\n");
        }

        System.out.println("data save to..." + location);
        writer.close();
    }

    private static void pushData() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("C:\\Users\\loyd_\\Desktop\\catDatabase.txt"));
        if (hasData()) return;
        for (String cat : cats) {
            writer.write(cat + "\n");
        }

        System.out.println("data exported...");
        writer.close();
    }

    private static void instruction() {
        System.out.println("Select an option:\n0\tPrint\n1\tAdd\n2\tDelete\n3\tModify\n4\tSearch\n5\tCapitalize\n6\tInstruction\n7\tImport\n8\tExport\n9\tQuit");
    }

    private static void capitalizeName() {
        if (hasData()) return;
        System.out.println("Enter name to capitalize: ");
        String name = scanner.nextLine();
        if (onFile(name)) {
            cats.set(position(name), name.toUpperCase());
            printName();
        } else {
            System.out.println(name + " doesn't exist");
        }
    }

    private static void searchName() {
        if (hasData()) return;
        System.out.println("Enter name to search: ");
        String name = scanner.nextLine();
        if (onFile(name)) {
            System.out.println(name + " is currently on file and it's on number " + (position(name) + 1));
        } else {
            System.out.println(name + " doesn't exist");
        }
    }

    private static void modifyName() {
        if (hasData()) return;
        System.out.println("Select name to modify:");
        printName();
        String oldName = scanner.nextLine();
        if (onFile(oldName)) {
            System.out.println("Enter new name:");
            String newName = scanner.nextLine();
            cats.set(position(oldName), newName);
            System.out.println(oldName + " renamed to " + newName);
        } else {
            System.out.println(oldName + " not exist");
        }
    }

    private static boolean hasData() {
        if (cats.size() <= 0) {
            System.out.println("No data found, please add to continue ");
            return true;
        }
        return false;
    }

    public static int position(String name) {
        return cats.indexOf(name);
    }

    public static void addName() {
        System.out.println("Enter names and click enter twice when done");
        if (cats.size() <= 100) {
            addingNames();
        } else {
            System.out.println("You can only add 100 data on this file. You have " + cats.size());
        }

    }

    public static void addingNames() {
        while (isFileMax()) {
            String response = scanner.nextLine();
            if (response.equals("")) {
                return;
            }
            if (!onFile(response)) {
                cats.add(response);
                System.out.println(response + " added");
            } else {
                System.out.println(response + " already exist.");
            }
        }
    }

    public static void printName() {
        System.out.println("You have " + cats.size() + " cat/s.");
        for (int x = 0; x < cats.size(); x++) {
            System.out.println(x + 1 + " - " + cats.get(x));
        }
    }

    public static void deleteName() {
        if (hasData()) return;
        System.out.println("Select name to delete");
        printName();
        String name = scanner.nextLine();
        if (onFile(name)) {
            cats.remove(name);
            System.out.println(name + " deleted");
        } else {
            System.out.println(name + " does not exist!");
        }


    }

    private static boolean onFile(String name) {
        for (String cat : cats) {
            try {
                if (name.equals(cat)) {
                    return true;
                }
            } catch (Exception e) {
                System.out.print("");
            }
        }
        return false;
    }

    private static boolean isFileMax() {
        if (cats.size() >= 100) {
            System.out.println("max limit!!!!");
            return false;
        } else return true;
    }
}
