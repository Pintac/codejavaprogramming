package outoftheboxscenarios;

public class Person {
    private String name;
    private int age;

    public Person(String name) {
        this(name, 29);
        System.out.println("Person with 1 constructor called");
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println("Person with 2 constructor called");

    }
    

}
