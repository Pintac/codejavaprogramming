package outoftheboxscenarios.calculator;

public class TigerEarn {
    private int moneyEarn;

    public TigerEarn(int moneyEarn) {
        this.moneyEarn = moneyEarn;
    }

    public int getMoneyEarn() {
        return moneyEarn;
    }
}
