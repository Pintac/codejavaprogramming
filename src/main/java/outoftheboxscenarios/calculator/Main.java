package outoftheboxscenarios.calculator;

public class Main {
    public static void main(String[] args) {
        PantherEarn pantherEarn = new PantherEarn(12);
        TigerEarn tigerEarn = new TigerEarn(8);

        Calculator calculator = new Calculator(pantherEarn,tigerEarn);
        calculator.totalEarn();

    }

}
