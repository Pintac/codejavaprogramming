package outoftheboxscenarios.calculator;

public class Calculator {
    private PantherEarn pantherEarn;
    private TigerEarn tigerEarn;

    public Calculator(PantherEarn pantherEarn, TigerEarn tigerEarn) {
        this.pantherEarn = pantherEarn;
        this.tigerEarn = tigerEarn;
    }

    public void totalEarn() {
        int total = pantherEarn.getMoneyEarn() + tigerEarn.getMoneyEarn();
        System.out.println("Total is " + total);
    }
}
