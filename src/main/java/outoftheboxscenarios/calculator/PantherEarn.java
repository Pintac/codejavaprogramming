package outoftheboxscenarios.calculator;

public class PantherEarn {
    private int moneyEarn;

    public PantherEarn(int moneyEarn) {
        this.moneyEarn = moneyEarn;
    }

    public int getMoneyEarn() {
        return moneyEarn;
    }
}
