package outoftheboxscenarios;

public class CallPerson extends Person{
    private int height;
    private int weight;
    private String gender;

    public CallPerson() {
        this(6);
    }

    public CallPerson(int height) {
        this(height,123);
        System.out.println("CallPerson with 1 constructor called");

    }

    public CallPerson(int height, int weight) {
        this(height,weight,"Male");
        System.out.println("CallPerson with 2 constructor called");

    }

    public CallPerson(int height, int weight, String gender) {
        super("Lloyd",28);
        System.out.println("CallPerson with 3 constructor called");

        this.height = height;
        this.weight = weight;
        this.gender = gender;
    }
}
