package outoftheboxscenarios;

class Dog {
    private static String name;

    public Dog(String name) {
        Dog.name = name;
    }

    public void printName(){
        System.out.println("name= "+name);
    }
}
public class Main{
    public static void main(String[] args) {
        Dog tiger = new Dog("Tiger");
        Dog panther = new Dog("Panther");
        tiger.printName();
        panther.printName();
    }
}