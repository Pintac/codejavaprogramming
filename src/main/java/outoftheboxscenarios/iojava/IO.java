package outoftheboxscenarios.iojava;

import java.io.*;
import java.util.Scanner;

public class IO {
    private static final Scanner scanner = new Scanner(System.in);
    private static final String readFromThisPath = "C:\\Users\\loyd_\\Desktop\\Sample.txt";
    private static final String writeToThisPath = "C:\\Users\\loyd_\\Desktop\\X.txt";

    public static void main(String[] args) throws IOException {
        /*String[] names = {"Lloyd", "Edith", "Tiger", "Panther","Ginger"};
        for (String name : names) {
            System.out.println(name);
        }*/
        //read(writeToThisPath);
        write(writeToThisPath,writeThis());
    }

    private static String writeThis() {
        System.out.println("Enter Text to write\r");
        String text = scanner.nextLine();
        return text;
    }

    public static String read(String path) throws IOException {
        String line;
        BufferedReader br = new BufferedReader(new FileReader(new File(path)));
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }
        return null;
    }

    public static void write(String path, String text) throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        writer.write(text);
        System.out.println("Done!! " + text + " has been updated to " + path + " file");
        writer.close();
    }
}
