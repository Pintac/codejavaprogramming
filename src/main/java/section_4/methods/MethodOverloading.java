package section_4.methods;

public class MethodOverloading {
    public static void main(String[] args) {
        int newScore = calculateScore("Lloyd", 3);
        System.out.println("New score is " + newScore);
        System.out.println("New score is "+ calculateScore(8));
        calculateScore();
    }
    public static int calculateScore() {
        System.out.println("PNo player name, no player score");
        return 0;
    }

    public static int calculateScore(int score) {
        System.out.println("Unnamed player scored " + score + " points");
        return score * 1000;
    }
    public static int calculateScore(String playerName, int score) {
        System.out.println("Player " + playerName + " score " + score + " points");
        return score * 1000;
    }
}
