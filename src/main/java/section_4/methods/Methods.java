package section_4.methods;

public class Methods {
    public static void main(String[] args) {
        int highScore = calculateScore(true, 800, 1000, 100);
        calculateScore(true, 1, 1, 1);
        System.out.println("Your final score was " + highScore);

        int highScorePosition = calculateHighScorePosition(1000);
        displayHigheScorePosition("Lloyd", highScorePosition);

        highScorePosition = calculateHighScorePosition(900);
        displayHigheScorePosition("Edith", highScorePosition);

        highScorePosition = calculateHighScorePosition(400);
        displayHigheScorePosition("Tiger", highScorePosition);

        highScorePosition = calculateHighScorePosition(1);
        displayHigheScorePosition("Panther", highScorePosition);
    }

    public static int calculateScore(boolean gameOver, int score, int levelCompleted, int bonus) {
        if (gameOver) {
            int finalScore = score + (levelCompleted * bonus);
            finalScore += 1000;
            return finalScore;
        }
        return -1;
    }

    public static void displayHigheScorePosition(String playersName, int highScorePosition) {
        System.out.println(playersName + " managed to get into position " + highScorePosition + " on the high score table");
    }

    public static int calculateHighScorePosition(int playerScore) {
        int position;
        if (playerScore >= 1000) {
            position = 1;
        } else if (playerScore >= 500) {
            position = 2;
        } else if (playerScore >= 100) {
            position = 3;
        } else {
            position = 4;
        }
        return position;
    }
}
