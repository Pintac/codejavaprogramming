package section_4.xcodingexercise;

public class MegaBytesConverter {
    public static void printMegaBytesAndKiloBytes(int kiloByte){
        if(kiloByte < 0 ){
            System.out.println("Invalid Value");
        }else {
            int output = kiloByte / 1024;
            int remaining = kiloByte % 1024;
            System.out.println(kiloByte +" KB = "+ output +" MB and "+remaining+" MB");
        }
    }
    public static void main(String[] args) {
        printMegaBytesAndKiloBytes(2_500);
        printMegaBytesAndKiloBytes(695_656);
        printMegaBytesAndKiloBytes(5_000);
    }
}
