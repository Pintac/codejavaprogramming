package section_4.xcodingexercise;

public class EqualityPrinter {
    private static final String INVALID_VALUE_MESSAGE ="Invalid Value";
    public static void printEqual(int numOne, int numTwo, int numThree){

        if(numOne<0||numTwo<0||numThree<0){
            System.out.println(INVALID_VALUE_MESSAGE);
        }else if(numOne==numTwo && numOne==numThree && numTwo==numThree){
            System.out.println("All numbers are equal");
        }else if(numOne==numTwo || numOne==numThree || numTwo==numThree){
            System.out.println("Neither all are equal or different");
        }else {
            System.out.println("All numbers are different");
        }
    }

    public static void main(String[] args) {
        printEqual(1,1,1);
        printEqual(1,1,2);
        printEqual(-1,-1,-2);
        printEqual(1,3,2);

    }
}
