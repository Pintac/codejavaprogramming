package section_4.xcodingexercise;

public class BarkingDog {
    public static boolean shouldWakeUp(boolean barking, int hourOfDay) {
        if (hourOfDay < 0 || hourOfDay > 23) {
            return false;
        }
        return barking && (hourOfDay < 8 || hourOfDay > 22);
    }

    public static void main(String[] args) {
        System.out.println(shouldWakeUp(true, 8));
        System.out.println(shouldWakeUp(true, 4));
        System.out.println(shouldWakeUp(true, -5));
        System.out.println(shouldWakeUp(true, 56));
    }
}
