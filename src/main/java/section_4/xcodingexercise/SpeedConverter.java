package section_4.xcodingexercise;

public class SpeedConverter {
    public static void main(String[] args) {
        double number = 1.5;
        System.out.println(toMilesPerHour(number));
        printConversion(number);
    }

    public static long toMilesPerHour(double kilometersPerHour) {
        if (kilometersPerHour < 0) {
            return -1;
        } else return Math.round(kilometersPerHour / 1.609);
    }
    public static void printConversion(double kilometersPerHour){
        if(kilometersPerHour <0){
            System.out.println("Invalid Value");
        }else {
            long milesPerHours = toMilesPerHour(kilometersPerHour);
            System.out.println(kilometersPerHour + " km/h = "+ milesPerHours+ " miles/hour");
        }
    }
}
