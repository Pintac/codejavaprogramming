package section_4.xcodingexercise;

public class DecimalComparator {
    public static boolean areEqualByThreeDecimalPlaces(double firstNumber, double secondNumber){
        int firstNumberCastToInt = (int)(1000 * firstNumber);
        int secondNumberCastToInt = (int)(1000 * secondNumber);
        if(firstNumberCastToInt==secondNumberCastToInt){
            return true;
        }else {
            return false;
        }
    }

    public static void main(String[] args) {
        System.out.println(areEqualByThreeDecimalPlaces(-3.1756,-3.175));
        System.out.println(areEqualByThreeDecimalPlaces(3.175,3.176));
        System.out.println(areEqualByThreeDecimalPlaces(3.0,3.0));
        System.out.println(areEqualByThreeDecimalPlaces(-3.123,3.123));
    }
}
