package section_4.xcodingexercise;

public class TeenNumberChecker {
    public static boolean hasTeen(int parOne, int parTwo, int parThree) {
        if (isTeen(parOne) || isTeen(parTwo) || (isTeen(parThree))) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        System.out.println(hasTeen(9, 99, 19));
        System.out.println(hasTeen(23, 15, 42));
        System.out.println(hasTeen(22, 23, 34));
        System.out.println(isTeen(9));
        System.out.println(isTeen(13));
    }

    public static boolean isTeen(int parOne) {
        if (parOne <= 12 || parOne >= 20) {
            return false;
        } else {
            return true;
        }
    }
}
