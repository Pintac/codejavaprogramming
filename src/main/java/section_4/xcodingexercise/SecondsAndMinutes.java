package section_4.xcodingexercise;

public class SecondsAndMinutes {
    private static final String INVALID_VALUE_MESSAGE = "Invalid Value!!";
    private static final int HOURS = 60;
    private static final int MINUTE = 60;
    private static String value = "";

    private static String getDurationString(int min, int sec) {

        if ((min >= 0) && (sec >= 0 && sec <= 59)) {
            int hours = min / HOURS;
            int minutes = min % HOURS;

            String hourString = hours + "h";
            if (hours < 10) {
                hourString = "0" + hourString;
            }

            String minuteString = minutes + "h";
            if (hours < 10) {
                minuteString = "0" + minuteString;
            }

            String secondString = sec + "s";
            if (sec < 10) {
                secondString = "0" + secondString;
            }
            value = hourString + " " + minuteString + " " + secondString + " ";
        } else {
            value = INVALID_VALUE_MESSAGE;
        }
        return value;
    }

    private static String getDurationString(int sec) {
        String value;
        if (sec >= 0) {
            int minutes = sec / MINUTE;
            int seconds = sec % MINUTE;
            value = getDurationString(minutes, seconds);
        } else {
            value = INVALID_VALUE_MESSAGE;
        }
        return value;
    }

    public static void main(String[] args) {
        System.out.println(getDurationString(5, 45));
        System.out.println(getDurationString(639456));
        System.out.println(getDurationString(-41));
        System.out.println(getDurationString(65,9));


    }
}
