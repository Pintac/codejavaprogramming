package section_4.xcodingexercise;

public class EqualSumChecker {
    public static boolean hasEqualSum(int parOne, int parTwo, int parThree) {
        int sum = parOne + parTwo;
        if (sum == parThree) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        System.out.println(hasEqualSum(1, 1, 1));
    }
}
