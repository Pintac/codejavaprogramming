package section_4.xcodingexercise;

public class MinutesToYearAndDays {
    private static final String INVALID_MESSAGE = "Invalid Value";
    public static void minutesToYearsAndDays(long minutes){
        if(minutes<0){
            System.out.println(INVALID_MESSAGE);
        }else{
            long years = minutes/525600l;
            long minutesRemaining = minutes%525600l;
            long days=0;
            if(minutesRemaining>=1440l){
                days = minutesRemaining/1440l;
            }
            System.out.println(minutes+" min = "+years+" y and "+days+" d");
        }
    }

    public static void main(String[] args) {
        minutesToYearsAndDays(525600);
        minutesToYearsAndDays(1051200);
        minutesToYearsAndDays(561600);
    }
}
