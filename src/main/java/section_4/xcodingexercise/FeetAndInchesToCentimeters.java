package section_4.xcodingexercise;

public class FeetAndInchesToCentimeters {
    public static double calcFeetAndInchesToCentimeters(double feet, double inches) {
        if ((feet >= 0) && (inches >= 0 && inches <= 12)) {
            double centimeters = (feet * 12 + inches) * 2.54;
            System.out.println(feet + " feet and " + inches + " inches is " + centimeters);
            return centimeters;
        } else {
            System.out.print("Invalid Parameters ");
            return -1;
        }
    }

    public static double calcFeetAndInchesToCentimeters(double inches) {
        if (inches >= 0) {
            int feet = (int) (inches / 12);
            int inch = (int) (inches % 12);
            System.out.println(inches + " inches is equal to " + feet + " feet and  " + inch + " inches");
            return calcFeetAndInchesToCentimeters(feet, inch);
        } else {
            System.out.print("Invalid Parameters ");
            return -1;
        }
    }

    public static void main(String[] args) {
      calcFeetAndInchesToCentimeters(157);
    }
}
