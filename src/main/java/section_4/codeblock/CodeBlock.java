package section_4.codeblock;

import java.util.Random;
import java.util.Scanner;

public class CodeBlock {
    static String HAND;
    static String PLAYER;
    static String COMPUTER = "Computer";
    static Scanner sc = new Scanner(System.in);
    static String win;

    public static void main(String[] args) {
        playGame();
    }

    public static void playGame() {

        getPLayer();
        for (int round = 1; round <= 3; round++) {
            System.out.println("Round " + round);
            winner(yourHand(), computerHand());
        }
    }

    public static String yourHand() {
        System.out.println("\nChoose 'ROCK', 'PAPER', 'SCISSOR'");
        HAND = sc.nextLine().toUpperCase();
        if ((HAND.equals("ROCK")) || (HAND.equals("PAPER")) || (HAND.equals("SCISSOR"))) {
            System.out.println(PLAYER + " enter " + HAND);
        } else {
            System.out.println("Invalid hand");
        }
        return HAND;
    }

    public static String computerHand() {
        Random random = new Random();
        String[] computerized = {"PAPER", "ROCK", "SCISSOR"};
        String cHand = (computerized[random.nextInt(3)]);
        System.out.println(COMPUTER + " enter " + cHand);
        return cHand;
    }

    public static void winner(String player1, String computer) {

        if ((player1.equals("PAPER")) && ((computer.equals("ROCK"))) || ((player1.equals("ROCK")) && ((computer.equals("SCISSOR"))) || (player1.equals("SCISSOR") && (computer.equals("PAPER"))))) {
            System.out.println(PLAYER + " won\n");
            win =PLAYER;
        } else if (player1.equals(computer)) {
            System.out.println("Its'a Draw\n");
        } else {
            System.out.println("Computer WON\n");
            win="Computer";
        }
    }

    public static void getPLayer() {
        System.out.print("Your name please:\t");
        PLAYER = sc.nextLine();
    }
}
