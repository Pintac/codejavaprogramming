package section_6.classes.part1;

public class Main {
    public static void main(String[] args) {
        Car porsche = new Car();
        Car holden = new Car();
        Car toyota = new Car();

        porsche.setModel("911");
        System.out.println("Model is "+porsche.getModel());

        toyota.setModel("Yaris");
        System.out.println("Model is "+toyota.getModel());

        toyota.setColour("Red");
        System.out.println(toyota.getColour());
    }
}
