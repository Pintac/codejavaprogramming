package section_6.classes.part1;

public class Car {

    private int doors;
    private int wheels;
    private String model;
    private String engine;
    private String colour;

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
       this.doors = doors;
    }

    public int getWheels() {
        return wheels;
    }

    public void setWheels(int wheels) {
        this.wheels = wheels;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        String validModel = model.toLowerCase();
        if(validModel.equals("911")|| validModel.equals("commodore")){
            this.model = model;
        }else {
            this.model = " is not high end. Throwing car";
        }
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        if(colour.equals("Pink")||colour.equals("Blue")){
            this.colour=colour +" is not my favorite color";
        }else {
           this.colour = colour +" is a nice color";
        }
    }

}
