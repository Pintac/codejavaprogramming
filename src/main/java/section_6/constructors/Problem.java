package section_6.constructors;

public class Problem {
    public static void main(String[] args) {
        int[] array = {1,2,3,4,5,6,7,8,9,10};
        isEven(array);
    }

    public static void isEven(int[] list) {
        for (int i : list) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }
}






