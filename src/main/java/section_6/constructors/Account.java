package section_6.constructors;

public class Account {
    private int accountNumber;
    private double balance;
    private String customerName;
    private String email;
    private String phoneNumber;

    public Account() {
        this("Lloyd", "loyd_pintac@hotmail.com", "(813)516-0335");
        System.out.println("Empty constructor called");
    }

    public Account(String customerName, String email, String phoneNumber) {
        this(21365445, 1000, customerName, email, phoneNumber);
    }

    public Account(int accountNumber) {
        this.accountNumber = accountNumber;
        this.balance = 5000;
    }

    public Account(int accountNumber, double balance, String customerName, String email, String phoneNumber) {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.customerName = customerName;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void deposit(double amount) {
        if (amount > 0) {
            this.balance += amount;
            System.out.println("Deposit of " + amount + " processed. Your Balance is " + getBalance());
        } else {
            System.out.println("The amount you enter is invalid");
        }
    }

    public void withdraw(double amount) {
        if (amount > 0 && this.balance >= amount) {
            this.balance -= amount;
            System.out.println("Withdrawal of " + amount + " processed. Your Balance is " + getBalance());
        } else {
            System.out.println("Insufficient Balance\nWithdrawal of " + amount + " is NOT processed. Your Balance is " + getBalance());
        }
    }

    public void printStatement() {
        System.out.println("\n*************************************\n******ACCOUNT STATEMENT******");
        System.out.println("Customer Name: " + getCustomerName());
        System.out.println("Account Number: " + getAccountNumber());
        System.out.println("Email Address: " + getEmail());
        System.out.println("Phone Number: " + getPhoneNumber());
        System.out.println("Current Balance: " + getBalance());
    }

}
