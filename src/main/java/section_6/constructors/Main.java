package section_6.constructors;

import java.util.Scanner;

public class Main {
    public static Scanner sc = new Scanner(System.in);
    public static Account Chase = new Account(1);

    public static void main(String[] args) {
        Chase.setEmail("loyd_pintac@yahoo.com");
        Chase.setPhoneNumber("(813)516-0335");
      /*  Account BOA = new Account();
        Account WellsFargo = new Account(12366555,10_000, "Edith Pintac","edith_pintac@yahoo.com","(956)212-2242");
        Account BBnT = new Account("tiger","emailYaho","45454354");*/
        if(isCorrectAccountNumber()){
            prompter();
        }else {
            System.out.println("Incorrect account number!!");
        }
    }
    public static boolean isCorrectAccountNumber(){
        int accountNumber;
        System.out.println("Enter your account number");
        accountNumber = sc.nextInt();
        return Chase.getAccountNumber() ==accountNumber;
    }

    public static void prompter() {
        boolean stopper = true;
        while (stopper) {
            System.out.println("\n\nPlease select an options:\nA\tWithdraw\nB\tDeposit\nC\tName" +
                    "\nD\tCheck Balance\nR\tAdd Name\nS\tSummary\nQ\tQuit");
            String choice = sc.next().toUpperCase();
            switch (choice) {
                case "A":
                    withdraw();
                    break;
                case "B":
                    deposit();
                    break;
                case "C":
                    name();
                    break;
                case "D":
                    balance();
                    break;
                case "R":
                    addName();
                    break;
                case "S":
                    summary();
                    break;
                case "Q":
                    System.out.println("Goodbye~~ ");
                    stopper =false;
                    break;
                default:
                    System.out.println("Wrong Input! Try again");
            }
        }
    }

    private static void summary() {
        System.out.println("Customer Name:\t"+ Chase.getCustomerName()+"\n" +
                "Account Number:\t"+Chase.getAccountNumber()+"\n" +
                "Balance Amount:\t"+Chase.getBalance()+"\n" +
                "Email:\t"+Chase.getEmail()+"\n" +
                "Phone Number:\t`"+Chase.getPhoneNumber());
    }

    private static void addName() {
        sc.nextLine();
        System.out.println("Enter name");
        String name = sc.nextLine();
        Chase.setCustomerName(name);
    }

    public static void withdraw() {
        System.out.println("Enter amount to withdraw");
        int amount = sc.nextInt();
        Chase.withdraw(amount);
        sc.nextLine();
    }

    public static void deposit() {
        System.out.println("Enter amount to deposit");
        int amount = sc.nextInt();
        Chase.deposit(amount);
    }

    public static void name() {
        System.out.println("Your name is "+Chase.getCustomerName());
    }

    public static void balance() {
        System.out.println("Current balance is "+Chase.getBalance());
    }
}