package section_6.xcodingchallenge.sectionchallenge;

public class ComplexNumber {
    private double real, imaginary;

    public ComplexNumber(double real, double imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }

    public double getReal() {
        return real;
    }

    public double getImaginary() {
        return imaginary;
    }

    public void add(double real, double imaginary) {
        this.real += real;
        this.imaginary += imaginary;
    }

    public void add(ComplexNumber complexNumber) {
        double real1 = complexNumber.getReal();
        double imaginary1 = complexNumber.getImaginary();

        this.real += real1;
        this.imaginary += imaginary1;
    }

    public void subtract(double real, double imaginary) {
        this.real -= real;
        this.imaginary -= imaginary;
    }

    public void subtract(ComplexNumber complexNumber) {
        double real1 = complexNumber.getReal();
        double imaginary1 = complexNumber.getImaginary();

        this.real -= real1;
        this.imaginary -= imaginary1;
    }

}
