package section_6.xcodingchallenge.sectionchallenge;

public class SimpleCalculator {
    private double firstNumber;
    private double secondNumber;

    public double getFirstNumber(){
        return firstNumber;
    }
    public double getSecondNumber(){
        return secondNumber;
    }
    public void setFirstNumber(double firstNumber){
        this.firstNumber = firstNumber;
    }
    public void setSecondNumber(double secondNumber){
        this.secondNumber = secondNumber;
    }
    public double getAdditionResult(){
        return firstNumber+this.secondNumber;
    }
    public double getSubtractionResult(){
        return firstNumber-this.secondNumber;
    }
    public double getMultiplicationResult(){
        return firstNumber*this.secondNumber;
    }
    public double getDivisionResult(){
        if(this.secondNumber==0){
            return 0;
        }else {
            return firstNumber/this.secondNumber;
        }
    }
}
