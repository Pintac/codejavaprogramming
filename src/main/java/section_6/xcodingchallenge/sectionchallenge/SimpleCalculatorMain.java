package section_6.xcodingchallenge.sectionchallenge;

public class SimpleCalculatorMain {
   public static SimpleCalculator simpleCalculator = new SimpleCalculator();

    public static void main(String[] args) {
        simpleCalculator.setFirstNumber(5.0);
        simpleCalculator.setSecondNumber(4);
        System.out.println("add= "+simpleCalculator.getAdditionResult());
        System.out.println("subtract= "+simpleCalculator.getSubtractionResult());
        simpleCalculator.setFirstNumber(5.25);
        simpleCalculator.setFirstNumber(0);
        System.out.println("multiply= "+simpleCalculator.getMultiplicationResult());
        System.out.println("divide= "+simpleCalculator.getDivisionResult());
    }
}
