package section_6.xcodingchallenge.sectionchallenge;

public class VipCustomer {

    private String name;
    private double creditLimit;
    private String emailAddress;

    public VipCustomer() {
        this("Lloyd Pintac",80_000,"loyd_pintac@yahoo.com");
    }

    public VipCustomer(double creditLimit, String emailAddress) {
       this("Edith Serna",creditLimit,emailAddress);
    }

    public VipCustomer(String name, double creditLimit, String emailAddress) {
        this.name = name;
        this.creditLimit = creditLimit;
        this.emailAddress = emailAddress;
    }

    public String getName() {
        return name;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public String getEmailAddress() {
        return emailAddress;
    }
}
