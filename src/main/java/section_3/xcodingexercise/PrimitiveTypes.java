package section_3.xcodingexercise;

public class PrimitiveTypes {
    public static void main(String[] args) {
        byte byteVariable = 10;
        short shortVariable = 20;
        int intVariable = 50;

        long longVariable = 50000L + (byteVariable+shortVariable+intVariable * 10L);
        System.out.println(longVariable);
        short shortTotal = (short)(50000 + (byteVariable + shortVariable +intVariable));
        System.out.println(shortTotal);
    }
}
