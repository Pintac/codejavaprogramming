package section_3.xcodingexercise;

public class WeightConverter {
    public static void main(String[] args) {
        double numberOfPounds = 200d;
        double KG = 0.45359237d;
        double toKilograms = numberOfPounds *KG;
        System.out.println("Number of Kilograms for the given lbs of "+numberOfPounds+" is "+toKilograms);
    }
}
