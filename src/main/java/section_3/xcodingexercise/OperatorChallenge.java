package section_3.xcodingexercise;

public class OperatorChallenge {
    public static void main(String[] args) {
        double myDoubleValue = 20.00d;
        double mySecondDoubleValue = 80.00d;
        double sum = (myDoubleValue + mySecondDoubleValue) * 100.00d;
        System.out.println("My sum is "+sum);
        double remainder = sum % 40.00d;
        System.out.println("My remainder is "+remainder);
        boolean myBooleanVariable = remainder == 0 ? true : false;
        System.out.println("my boolean is "+myBooleanVariable);
        if(!myBooleanVariable){
            System.out.println("Got some remainder");
        }
    }
}
