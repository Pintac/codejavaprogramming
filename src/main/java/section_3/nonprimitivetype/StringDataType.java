package section_3.nonprimitivetype;

public class StringDataType {

    public static void main(java.lang.String... args) {
        String myString = "This is a string";
        System.out.println("My string is equal to "+myString);
        myString = myString +" more Strings";
        System.out.println(myString);
        myString = myString+ " \ua433";
        System.out.println(myString);
        String numberString = "250.55";
        numberString = numberString + "49.45";
        System.out.println(numberString);
        String lastString = "10";
        int myInt = 50;
        lastString = lastString + myInt;
        System.out.println("LastString is equal to "+ lastString);
        System.out.println(myInt +"121");
    }
}
