package section_3.operators;

public class IfThenElse {

    public static void main(String[] args) {
        boolean isALien = data();
        if (!isALien) {
            System.out.println("It is not an alien!");
        } else {
            System.out.println("It's an alien");
        }
        int score = 69;
        
        if (score == 100) {
            System.out.println("You have grade A");
        } else if (score >= 90) {
            System.out.println("You have grade B");
        }else if (score >= 80) {
            System.out.println("You have grade C");
        }else if (score >= 70) {
            System.out.println("You have grade D");
        }else if (score >= 60) {
            System.out.println("You have grade E");
        }else {
            System.out.println("You failed!");
        }
    }
    public static boolean data(){
        return true;
    }
}


