package section_3.operators;

public class Operators {
    public static void main(String[] args) {
        int result = 1 + 2;
        //= and + are the operators
        //1 and 2 are the operands
        //1 + 2 is the expressions
        System.out.println("Result of 1 + 2 is " + result);
        result++;
        System.out.println("Result of result++ is " + result);
        result += 5;
        System.out.println("Result of +=5 is " + result);
        boolean isBoxPresent = true;
        if (isBoxPresent) {
            System.out.println("OK");
        }
        int ageOfClient = 21;
        boolean isIt = ageOfClient == 21 ? true : false;
        System.out.println(isIt);
    }

}
