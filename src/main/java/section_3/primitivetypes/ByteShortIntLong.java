package section_3.primitivetypes;


public class ByteShortIntLong {
    public static void main(String[] args) {

        long value = 1_212_343_344_432L;
        int myValue =2_324_324;

        long myMinLongValue = Long.MIN_VALUE;
        long myMaxLongValue = Long.MAX_VALUE;

        int myMinIntValue = Integer.MIN_VALUE;
        int myMaxIntValue = Integer.MAX_VALUE;

        short myMinShortValue = Short.MIN_VALUE;
        short myMaxShortValue = Short.MAX_VALUE;

        byte myMinByteValue = Byte.MIN_VALUE;
        byte myMaxByteValue = Byte.MAX_VALUE;

        System.out.println("Long Minimum Value = "+myMinLongValue);
        System.out.println("Long Maximun Value = "+myMaxLongValue);

        System.out.println("Integer Minimum Value = "+myMinIntValue);
        System.out.println("Integer Maximun Value = "+myMaxIntValue);
        System.out.println("Busted MAX Value = "+(myMaxIntValue+1));
        System.out.println("Busted MIN Value = "+(myMaxIntValue-1));

        System.out.println("Short Minimum Value = "+myMinShortValue);
        System.out.println("Short Maximun Value = "+myMaxShortValue);

        System.out.println("Byte Minimum Value = "+myMinByteValue);
        System.out.println("Byte Maximun Value = "+myMaxByteValue);

        byte myNewByteValue =(byte)(myMinByteValue /2);
    }
}
