package section_3.primitivetypes;

import java.sql.Time;
import java.time.LocalTime;

public class CharBoolean {
    public static void main(String[] args) {
        char myChar = '\u0298' + '\u4323';
        char myUnidecodeChar = '\u0298' + '\u4323' + '\u0298' + '\u4323';
        char a = '\u0298' + '\u3223';
        char b = '\u0298' + '\u4323' + '\u0298';
        char c = '\u0298' + '\u4323' + '\u0248' + '\u6623';
        char x = '\u0798' + '\u4323' + '\u0298' + '\u4323';
        char e = '\u0F3A';
        System.out.print(myChar);
        System.out.print(a);
        System.out.print(b);
        System.out.print(c);
        System.out.print(x);
        System.out.print(e);
        System.out.println(myUnidecodeChar);

        String lloyd = " \u023D" + "\u023D" + "\u0298" + "\u024E" + "\u0189";
        String jayson = " \u0134" + "\u00C6" + "\u024E" + "\u015A" + "\u0150" + "\u014A" + "\u0F3C" + "\u0F3B";
        String pintac = " \u0F3A" + "\u0F3D" + "\u01A4" + "\u012C" + "\u014A" + "\u0162" + "\u0104" + "\u00C7" + "\u0F3C";

        String fullName = pintac + lloyd + jayson;
        System.out.println(fullName);
        System.out.println("Time is "+Time.valueOf(LocalTime.now()));

        boolean myTrueBolleanValue = true;
        boolean myFalseBolleanValue = false;

        boolean isCustomerOverTwentyOne = true;

        System.out.println(isCustomerOverTwentyOne);

    }
}
