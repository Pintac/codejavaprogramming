package section_3.primitivetypes;

public class FloatDouble {
    public static void main(String[] args) {

        double myMinDoubleValue = Double.MIN_VALUE;
        double myMaxDoubleValue = Double.MAX_VALUE;

        float myMinFloatValue = Float.MIN_VALUE;
        float myMaxFloatValue = Float.MAX_VALUE;

        System.out.println("Double Minimum Value = "+ myMinDoubleValue);
        System.out.println("Double Maximum Value = "+ myMaxDoubleValue);

        System.out.println("Float Minimum Value = "+ myMinFloatValue);
        System.out.println("Float Maximum Value = "+ myMaxFloatValue);

        int value = 5/3;
        int myIntValue;
        float myFloatValue = 5f /3f;
        double myDoubleValue = 5d / 3d;
        myIntValue =(int)myFloatValue;
        System.out.println("My Int Value = "+value);
        System.out.println("My New Int Value = "+myIntValue);
        System.out.println("My Double Value = "+myDoubleValue);
        System.out.println("My Float Value = "+myFloatValue);

    }
}
