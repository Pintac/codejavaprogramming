package section_8.arrays;

import java.util.*;

public class Number {
    private static Scanner scanner = new Scanner(System.in);
    private static Random random = new Random();

    public static void main(String[] args) {
        int[] myArray = getNumbers(5);
        printNumbers(sorter(myArray));
    }

    private static int[] sorter(int[] number) {
        int[] sortedNumbers = Arrays.copyOf(number, number.length);
        boolean flag = true;
        int temp = 0;
        while (flag) {
            flag = false;
            for (int x = 0; x < sortedNumbers.length - 1; x++) {
                if (sortedNumbers[x] > sortedNumbers[x + 1]) {
                    temp = sortedNumbers[x];
                    sortedNumbers[x] = sortedNumbers[x + 1];
                    sortedNumbers[x + 1] = temp;
                    flag = true;
                }
            }
        }
        return sortedNumbers;
    }

    private static void printNumbers(int[] numbers) {
        System.out.println("Sorting the Computer Generated Numbers");
        for (int x : numbers) {
            System.out.println("Printing " + x);
        }
    }

    private static int[] getNumbers(int numbers) {
        System.out.println("Auto Generated Numbers");
        int[] values = new int[numbers];
        for (int x = 0; x < values.length; x++) {
            values[x] = random.nextInt(100);
            System.out.println("Element "+x+" has "+values[x]);
        }
        System.out.println("____________________________");
        return values;
    }
}
