package section_8.arrays;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] myIntegers = getIntegers(5);

        for (int x = 0; x < myIntegers.length; x++) {
            System.out.println("Element " + x + ", typed value was " + myIntegers[x]);
        }
        double sum = getSum(myIntegers);
        double avg = sum/myIntegers.length;
        System.out.println("Sum of all numbers are " + sum);
        System.out.println("Average of the numbers are " + avg);
    }

    public static int[] getIntegers(int number) {
        System.out.println("Enter " + number + " integer values. \r");
        int sum = 0;
        int[] values = new int[number];

        for (int x = 0; x < values.length; x++) {
            values[x] = scanner.nextInt();
        }

        return values;
    }

    public static double getSum(int[] numbers) {
        double sum = 0;
        for (int x = 0; x < numbers.length; x++) {
            sum += numbers[x];
        }
        return sum;
    }
}
