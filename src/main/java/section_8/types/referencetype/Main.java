package section_8.types.referencetype;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] myIntArray = new int[5];
        int[] anontherArray = myIntArray;

        System.out.println("myIntArray = " + Arrays.toString(myIntArray));
        System.out.println("anontherArray = " + Arrays.toString(anontherArray));

        anontherArray[0] = 1;

        System.out.println("After change:\nmyIntArray = " + Arrays.toString(myIntArray));
        System.out.println("anontherArray = " + Arrays.toString(anontherArray));

        anontherArray = new int[]{3, 4, 5, 6, 7, 8};
        modifiedArray(myIntArray);

        System.out.println("After modify:\nmyIntArray = " + Arrays.toString(myIntArray));
        System.out.println("anontherArray = " + Arrays.toString(anontherArray));
    }

    private static void modifiedArray(int[] array) {
        array[0] = 2;
    }
}
