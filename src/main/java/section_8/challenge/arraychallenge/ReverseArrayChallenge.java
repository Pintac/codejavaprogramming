package section_8.challenge.arraychallenge;

import java.util.Arrays;

public class ReverseArrayChallenge {
    public static void main(String[] args) {
        int[] array = {21,23,45,32,45,11,20,320,1};
        System.out.println(Arrays.toString(array));
        reverse(array);
    }

    private static void reverse(int[]array) {
       for(int x = array.length-1;x>=0;x--){
           System.out.print(array[x]+" ");
       }
    }
}
