package section_8.challenge.arraychallenge;

import java.util.Scanner;

public class MinElementChallenge {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter count:\t");
        int count = SCANNER.nextInt();
        findMin(readIntegers(count));
    }

    private static int[] readIntegers(int count) {
        int[] array = new int[count];
        System.out.println("Enter " + count + " numbers\r");
        for (int x = 0; x < array.length; x++) {
            array[x] = SCANNER.nextInt();
        }
        return array;
    }

    private static int findMin(int[] array) {
        int minVal = Integer.MAX_VALUE;
        for (int x = 0; x < array.length; x++) {
           int value = array[x];
           if(value <minVal){
               minVal=value;
           }
        }
        System.out.println("The minimum value is " + minVal);
        return minVal;
    }
}
