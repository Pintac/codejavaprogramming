package section_8.challenge.arraysproject;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] myIntegers = getIntegers(5);
        int[] sorted = sortArrays(myIntegers);
        printArray(sorted);
    }

    public static int[] getIntegers(int capacity) {
        System.out.println("Enter " + capacity + " integer values:\r");
        int[] array = new int[capacity];
        for (int x = 0; x < array.length; x++) {
            array[x] = scanner.nextInt();
        }
        return array;
    }

    public static void printArray(int[] array) {
        for (int x = 0; x < array.length; x++) {
            System.out.println("Element " + x + " contents " + array[x]);
        }
    }

    public static int[] sortArrays(int[] array) {
        int[] sortedArray = Arrays.copyOf(array,array.length);
        boolean flag = true;
        int temp;
        while (flag) {
            flag = false;
            for (int x = 0; x < sortedArray.length - 1; x++) {
                if (sortedArray[x] > sortedArray[x + 1]) {
                    temp = sortedArray[x];
                    sortedArray[x] = sortedArray[x + 1];
                    sortedArray[x + 1] = temp;
                    flag = true;
                }
            }
        }
        return sortedArray;

    }
}
