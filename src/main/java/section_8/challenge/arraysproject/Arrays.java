package section_8.challenge.arraysproject;

public class Arrays {
    public static void main(String[] args) {
        int[] array = {95, 45, 32, 65, 12, 45};
        print(sorter(array));

    }


    private static int[] sorter(int[] array) {
        int[] sortedArray = java.util.Arrays.copyOf(array, array.length);
        boolean flag = true;
        int temp = 0;
        while (flag) {
            flag = false;
            for (int x = 0; x < sortedArray.length - 1; x++) {
                if (sortedArray[x] > sortedArray[x + 1]) {
                    temp = sortedArray[x];
                    sortedArray[x] = sortedArray[x + 1];
                    sortedArray[x + 1] = temp;
                    flag = true;
                }
            }
        }
        return sortedArray;
    }


    public static void print(int[] numbers) {
        for (int number : numbers) {
            System.out.println(number);
        }
    }
}
