package section_8.challenge.arraylistchallenge1.challenge;

import java.util.Scanner;

public class MobilePhone {
    public Scanner scanner = new Scanner(System.in);
    Contacts contacts = new Contacts();

    public void addContacts(String newContact) {
        if(contactOnFiles(newContact)){

        }else {
            contacts.name.add(newContact);
            System.out.println(newContact + " has been added to your contact lists");
        }

    }

    public void modifyContact(String currentContact, String newContac) {
        int position = findContact(currentContact);
        if (position >= 0) {
            modifyContact(position, newContac);
            System.out.println(currentContact + " has been modified to " + newContac);
        } else {
            prompt(currentContact, newContac);
        }
    }

    private void prompt(String currentContact, String newContact) {
        System.out.println(currentContact + " is not in in the contact list. Want to add " + currentContact + " and " + newContact + " anyway?\n Y\tYes\n N\tNo");
        String input = scanner.nextLine();
        switch (input.toUpperCase()) {
            case "Y":
                addContacts(currentContact);
                addContacts(newContact);
                break;
            case "N":
                break;
            default:
                System.out.println("Wrong option. Please enter a valid input");
                prompt(currentContact, newContact);
        }
    }

    private void prompt(String newContact) {
        System.out.println("Want to add " + newContact + "?\n Y\tYes\n N\tNo");
        String input = scanner.nextLine();
        switch (input.toUpperCase()) {
            case "Y":
                addContacts(newContact);
                break;
            case "N":
                break;
            default:
                System.out.println("Wrong option. Please enter a valid input");
                prompt(newContact);
        }
    }

    private void modifyContact(int position, String contactName) {
        contacts.name.set(position, contactName);
    }

    public void removeContact(String contact) {
        int position = findContact(contact);
        if(position >=0){
            deleteContact(contact);
        }else {
            System.out.println(contact + " can not be found on the list");
        }
    }

    private void deleteContact(String contact) {
        contacts.name.remove(contact);
        System.out.println(contact + " has been removed");
    }

    private int findContact(String contact) {
        return contacts.name.indexOf(contact);
    }

    public boolean contactOnFile(String contact) {
        int position = findContact(contact);
        if (position >= 0) {
            return true;
        }
        System.out.println(contact + " is not on the contact list");
        prompt(contact);
        return false;
    }
    public boolean contactOnFiles(String contact) {
        int position = findContact(contact);
        if (position >= 0) {
            System.out.println(contact +" already exist on your contact list");
            return true;
        }
        return false;
    }

    public void printContacts() {
        System.out.println("You have "+contacts.name.size()+" in your contact list.");
        for (int x = 0; x < contacts.name.size(); x++) {
            System.out.println(x + 1 + " " + contacts.name.get(x));
        }
    }
}
