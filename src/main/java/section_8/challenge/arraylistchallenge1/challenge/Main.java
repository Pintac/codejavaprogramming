package section_8.challenge.arraylistchallenge1.challenge;

import java.util.Scanner;

public class Main {
    public static Scanner scanner = new Scanner(System.in);
    public static MobilePhone mobilePhone = new MobilePhone();

    public static void main(String[] args) {
        printInstruction();
        boolean quit = false;
        while (!quit) {
            System.out.print("Enter your choice: ");
            int input = scanner.nextInt();
            scanner.nextLine();
            switch (input) {
                case 0:
                    printInstruction();
                    break;
                case 1:
                    addContact();
                    break;
                case 2:
                    modifyContact();
                    break;
                case 3:
                    removeContact();
                    break;
                case 4:
                    searchContact();
                    break;
                case 5:
                    printContactList();
                    break;
                case 6:
                    System.out.println("Thank you");
                    quit = true;
                    break;
            }
        }

    }

    private static void printInstruction() {
        System.out.println("Press\n\t0\tto print instruction" +
                "\n\t1\tto add new contacts" +
                "\n\t2\tto modify contacts" +
                "\n\t3\tto remove contacts" +
                "\n\t4\tto search contacts" +
                "\n\t5\tto print contacts" +
                "\n\t6\tto quit");
    }

    private static void addContact() {
        System.out.print("Enter new contact: ");
        mobilePhone.addContacts(scanner.nextLine());
    }

    private static void modifyContact() {
        System.out.println("Select contact to modify:");
        printContactList();
        String currentContact = scanner.nextLine();
        if (mobilePhone.contactOnFile(currentContact)) {

        } else {
            System.out.println("Cannot find " + currentContact + " on your contact list");
        }
        System.out.print("Enter new contact: ");
        String newContact = scanner.nextLine();
        if (mobilePhone.contactOnFiles(newContact)) {
        } else {
            mobilePhone.modifyContact(currentContact, newContact);
        }

    }

    private static void removeContact() {
        System.out.print("Enter contact to delete: ");
        printContactList();
        mobilePhone.removeContact(scanner.nextLine());
    }

    private static void searchContact() {
        System.out.print("Enter contact:");
        mobilePhone.contactOnFile(scanner.nextLine());
    }

    private static void printContactList() {
        mobilePhone.printContacts();
    }
}
