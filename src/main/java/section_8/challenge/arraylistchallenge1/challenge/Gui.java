package section_8.challenge.arraylistchallenge1.challenge;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Gui {
    public String[] cl = new String[]{};
    DefaultListCellRenderer listRenderer = new DefaultListCellRenderer();
    JFrame frame;
    JPanel panel;
    ArrayList<String> mycontact = new ArrayList<>();
    JComboBox<String> contacts = new JComboBox<>(cl);
    JButton addContactButton;
    JButton modifyContactButton;
    JButton removeContactButton;
    JButton printContactButton;
    JTextArea area = new JTextArea("My Contact List\n");
    int height = 30;
    int width = 90;

    public Gui() {
        listRenderer.setHorizontalAlignment(DefaultListCellRenderer.CENTER);
        frame = new JFrame("Contacts");
        frame.setLayout(null);
        frame.setSize(480, 600);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);

        area.setBounds(50, 280, 360, 250);
        area.setBackground(Color.darkGray);
        area.setForeground(Color.white);


        contacts.setBounds(150, 80, 155, 30);
        contacts.setRenderer(listRenderer);

        removeContactButton = new JButton("Remove");
        removeContactButton.setBounds(20, 20, width, height);
        removeContactButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String item = contacts.getItemAt(contacts.getSelectedIndex());
                int answer = JOptionPane.showConfirmDialog(frame, "Want to remove " + item + " ?");
                if (answer == JOptionPane.YES_OPTION) {
                    contacts.removeItem(item);
                }
            }
        });

        modifyContactButton = new JButton("Modify");
        modifyContactButton.setBounds(130, 20, width, height);
        modifyContactButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String currentItem = contacts.getItemAt(contacts.getSelectedIndex());
                String newItem = JOptionPane.showInputDialog("Modify " + currentItem + " to:");
                contacts.removeItem(currentItem);
                contacts.addItem(newItem);
            }
        });

        addContactButton = new JButton("Add");
        addContactButton.setBounds(240, 20, width, height);
        addContactButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String con = JOptionPane.showInputDialog("Add Contact");
                while (con.equals(null)) {
                    contacts.addItem("<empty>");
                }
                for (int x = 0; x < contacts.getItemCount(); x++) {
                    if (con.equals(contacts.getItemAt(x))) {
                        String s = JOptionPane.showInputDialog("Contact name already exists");
                        if (s.equals("")) {
                            contacts.removeItem(s);
                            contacts.addItem("<empty>");
                        }
                        contacts.addItem(s);
                        contacts.removeItem(con);
                    }
                }
                contacts.addItem(con);
            }
        });

        printContactButton = new JButton("Print");
        printContactButton.setBounds(350, 20, width, height);
        printContactButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int count = contacts.getItemCount();
                if (count == 0) {
                    area.setText("Please add contact, you have " + count + " contact on your list");
                } else {
                    area.setText("You have " + count + " contact/s on your list\n");
                    for (int x = 0; x < count; x++) {
                        String item = contacts.getItemAt(x);
                        area.append(contacts.getItemAt(x) + "\n");
                    }
                }
            }

        });

        frame.add(addContactButton);
        frame.add(contacts);
        frame.add(modifyContactButton);
        frame.add(printContactButton);
        frame.add(removeContactButton);
        frame.add(area);

        frame.setVisible(true);

    }

    public static void main(String[] args) {
        new Gui();
    }
}
