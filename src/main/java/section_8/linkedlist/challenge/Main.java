package section_8.linkedlist.challenge;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class Main {

    private static ArrayList<Album> albums = new ArrayList<>();

    public static void main(String[] args) {
        Album album = new Album("Folklore", "Taylor Swift");
        album.addSong("Guitar", 4.2);
        album.addSong("Girlfriend", 4.0);
        album.addSong("Seven", 3.6);
        album.addSong("Guitar", 5.1);
        album.addSong("Private", 3.53);
        albums.add(album);

        album = new Album("Rock", "Demi Lovato");
        album.addSong("Airplane", 4.23);
        album.addSong("Jacket", 4.1);
        album.addSong("Rose", 3.96);
        album.addSong("Bed of Roses", 5.0);
        album.addSong("Car", 3.44);
        album.addSong("Jetplane", 3.12);

        albums.add(album);

        LinkedList<Song> playList = new LinkedList<>();
        LinkedList<String> playList1 = new LinkedList<>();

        albums.get(0).addToPlayList("Jetplane", playList);
        albums.get(0).addToPlayList("Guitar", playList);
        albums.get(0).addToPlayList("New Kid", playList);
        albums.get(0).addToPlayList("Jacket", playList);
        albums.get(1).addToPlayList(2, playList);
        albums.get(1).addToPlayList(3, playList);
        albums.get(1).addToPlayList(3, playList);
        albums.get(1).addToPlayList(1, playList);
        albums.get(1).addToPlayList(23, playList);

        playList.add(new Song("Lloyd",3.43));
        System.out.println(playList);
        playList1.add("Hello");


        play(playList);

    }

    private static void play(LinkedList<Song> playList) {
        ListIterator<Song> listIterator = playList.listIterator();
        if (playList.size() == 0) {
            System.out.println("No Song in playlist");
            return;
        } else {
            System.out.println("Now playing " + listIterator.next().toString());
        }
    }
}
