package section_8.autoboxing;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class Edith {
    public static Scanner scanner = new Scanner(System.in);
    public static HashMap<String, Integer> person = new HashMap<>();
private String name;

    public Edith(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static void main(String[] args) {
        loadNameandAge();
        //printNameandAge();
        getAge();
readExcel();
    }

    private static void readExcel() {
//reading the value of 2nd row and 2nd column
        String vOutput=ReadCellData(2, 2);
        System.out.println(vOutput);
    }
    //method defined for reading a cell
    public static String ReadCellData(int vRow, int vColumn)
    {
        String value=null;          //variable for storing the cell value
        Workbook wb=null;           //initialize Workbook null
        try
        {
//reading data from a file in the form of bytes
            FileInputStream fis=new FileInputStream("C:\\demo\\EmployeeData.xlsx");
//constructs an XSSFWorkbook object, by buffering the whole stream into the memory
            wb=new XSSFWorkbook(fis);
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch(IOException e1)
        {
            e1.printStackTrace();
        }
        Sheet sheet=wb.getSheetAt(0);   //getting the XSSFSheet object at given index
        Row row=sheet.getRow(vRow); //returns the logical row
        Cell cell=row.getCell(vColumn); //getting the cell representing the given column
        value=cell.getStringCellValue();    //getting cell value
        return value;               //returns the cell value
    }


    private static void getAge() {
        System.out.println("Enter name");
        String name = scanner.nextLine();
        System.out.println(name + " age is "+person.get(name));
    }

    private static void printNameandAge() {
        System.out.println("Name\tAge\n*************");
        for (String names : person.keySet()) {
            System.out.println(names+"\t"+person.get(names));
        }
    }

    private static void loadNameandAge() {
        person.put("Lloyd", 29);
        person.put("Edith", 30);
        person.put("Panther", 1);
        person.put("Tiger", 84);
        person.put("Ginger", 61);
        person.put("Elote", 462);
        person.put("Kamote", 295);

    }

    private static void addNameAndAge() {
        System.out.println("Enter name:");
        String name = scanner.nextLine();
        System.out.println("Enter age");
        int age = scanner.nextInt();
        scanner.nextLine();
        person.put(name,age);
    }
}
