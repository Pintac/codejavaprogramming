package section_8.autoboxing;

import java.util.ArrayList;

public class RecapArrays {

    public static void main(String[] args) {
      /*  String[] strArray = new String[10];
        int[] intArray = new int[10];

        ArrayList<Edith> strArrayList = new ArrayList<Edith>();
        strArrayList.add(new Edith("Lloyd"));
        Integer integer = new Integer(54);
        Double aDouble = new Double(12.32);

        ArrayList<Integer> intArraylist = new ArrayList<Integer>();
        for(int i = 0; i<=10;i++){
          intArraylist.add(Integer.valueOf(i));
        }
        for(int i=0;i<=10;i++){
            System.out.println(i + " --> "+ intArraylist.get(i).intValue());
        }*/



        Integer myIntValue = 56;

        ArrayList<Double> myDoubleValues = new ArrayList<>();
        for(double i=0.0; i<=10.0;i+=0.3){
            myDoubleValues.add(i);
        }

        for(int i=0;i<myDoubleValues.size();i++){
            double value = myDoubleValues.get(i);
            System.out.println(i +" --> "+value);
        }
    }
}
