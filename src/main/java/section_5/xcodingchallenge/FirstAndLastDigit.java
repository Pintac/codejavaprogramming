package section_5.xcodingchallenge;

public class FirstAndLastDigit {
    public static int sumFirstAndLastDigit(int number) {
        int sum = 0;
        int lastDigit = number % 10;

        if (number < 0) {
            sum = -1;
        } else if (number > 0 && number < 10) {
            sum = number*2;
        } else {
            do {
                number /= 10;
            } while (number >= 10);
            sum = number + lastDigit;
        }

        return sum;
    }

    public static void main(String[] args) {
        System.out.println(sumFirstAndLastDigit(101));
    }
}
