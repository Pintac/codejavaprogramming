package section_5.xcodingchallenge;

public class DigitSum {
    public static int sumDigits(int number) {
        int sum = 0;
        if (number < 10 || number < 0) {
            return -1;
        } else {
            while (number>0){
                int digit = number%10;
                sum+=digit;
                number/=10;
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        int n = 1001;
        System.out.println("Sum of "+n+" number is " + sumDigits(n));

    }
}
