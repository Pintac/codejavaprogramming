package section_5.xcodingchallenge;

public class FlourPack {
    public static boolean canPack(int bigCount, int smallCount, int goal) throws ArithmeticException {
        if (bigCount < 0 || smallCount < 0 || goal < 0 || goal > bigCount * 5 + smallCount) {
            return false;
        } else {
            return goal % 5 == 0 ||smallCount>=goal%5;
        }
    }

    public static void main(String[] args) throws ArithmeticException {
        System.out.println(canPack(1, 0, 4));
        System.out.println(canPack(1, 0, 5));
        System.out.println(canPack(0, 5, 4));
    }
}
