package section_5.xcodingchallenge;

public class SumOdd {
    public static boolean isOdd(int number) {
        if ((number <= 0) || (number % 2 == 0)) {
            return false;
        } else {
            return true;
        }
    }

    public static void main(String[] args) {
        System.out.println(isOdd(3));
        System.out.println(sumOdd(100, 1000));
    }

    public static int sumOdd(int start, int end) {
        int sum = 0;
        if (start > 0 && end > 0 && start <= end) {
            for (int i = start; i <= end; i++) {
                if (isOdd(i))
                    sum += i;
            }
            return sum;
        }
        return -1;
    }
}
