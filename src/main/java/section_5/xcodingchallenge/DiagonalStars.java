package section_5.xcodingchallenge;

public class DiagonalStars {
    public static String printSquareStar(int number){
        String stars = "*";
        if(number<5){
            stars = "Invalid numbers";
        }else {
            for(int count = number; count >0;count--){
                System.out.print(stars);
            }
        }
        return stars;
    }

    public static void main(String[] args) {
printSquareStar(5);
    }
}
