package section_5.xcodingchallenge;

public class SwitchChar {
    public static void main(String[] args) {
        char value = 'D';
        switch (value) {
            case 'A':
                System.out.println("Value is A");
                break;
            case 'B':
                System.out.println("Value is B");
                break;
            case 'C':
            case 'D':
            case 'E':
                System.out.println(value + " was found");
                break;

            default:
                System.out.println("None of the above");
        }
        String month = "november";
        String MONTH = month.substring(0, 1).toUpperCase() + month.substring(1).toLowerCase();
        switch (MONTH) {
            case "January":
            case "February":
            case "March":
            case "April":
            case "May":
            case "June":
            case "July":
            case "August":
            case "September":
            case "October":
            case "November":
            case "December":
                System.out.println("Month is " + MONTH);
                break;
            default:
                System.out.println(month + " is not a month");
                break;
        }
    }
}
