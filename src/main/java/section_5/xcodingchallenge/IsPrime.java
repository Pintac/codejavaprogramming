package section_5.xcodingchallenge;

public class IsPrime {
    private static boolean isPrime(int number) {
        if (number == 1) {
            return false;
        }
        for (int x = 2; x <= number / 2; x++) {
            if (number % x == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int count = 0;
        for (int counter = 10; counter <= 100; counter++) {
            if(isPrime(counter)){
                count++;
                System.out.println("Number " + counter+ " is a prime number");
                if (count==10){
                    System.out.println("Ending the loop");
                    break;
                }
            }else {
                System.out.print("");
            }
        }
    }
}
