package section_5.xcodingchallenge;

public class SharedDigit {
    public static boolean hasSharedDigit(int num1, int num2) {
        if ((num1 < 10 || num1 > 99) || (num2 < 10 || num2 > 99)) {
            return false;
        }
        while (num1 != num2) {
            num1 %= 10;

            if (num2 % 10 != 0) {
                num2 /= 10;
            }
        }
        return num1 == num2;
    }

    public static void main(String[] args) {
        System.out.println(hasSharedDigit(39, 49));
    }
}
