package section_5.xcodingchallenge;

public class NumberOfDaysInMonth {
    public static boolean isLeapYear(int year) {
        if ((year < 1) || year > 9999) {
            return false;
        } else if (year % 4 == 0) {
            if (year % 100 == 0) {
                return year % 400 == 0;
            } else return true;
        } else return false;
    }

    public static int getDaysInMonth(int month, int year){
        if ((month < 1 || month >= 13) || (year < 0 || year > 9999)) {
            return -1;
        }
        else if (!isLeapYear(year)) {
            int february1 = 28;
            int pairMonth1 = 30;
            int oddMonth1= 31;
            switch (month) {
                case 1: case 3: case 5: case 7: case 8: case 10:case 12:
                    return oddMonth1;
                case 2:
                    return february1;
                default:
                    return pairMonth1;
            }
        }
        else {

            int oddMonth = 31;
            int pairMonth = 30;
            int february = 29;

            switch (month){
                case 1: case 3: case 5: case 7: case 8: case 10:case 12:
                    return oddMonth;
                case 2:
                    return february;
                default:
                    return pairMonth;
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(getDaysInMonth(1,2020));
    }
}
