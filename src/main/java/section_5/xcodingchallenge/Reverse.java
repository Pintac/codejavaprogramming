package section_5.xcodingchallenge;

public class Reverse {
    public static void reverseWord() {
        String words = "Hello";
        String reverse;
        for (int count = words.length() - 1; count >= 0; count--) {
            reverse = words.substring(count, count + 1);
            System.out.print(reverse.toUpperCase());
        }
    }

    public static StringBuffer reverseWord(String words) {
        StringBuffer reverse = new StringBuffer();
        return reverse.append(words).reverse();

    }

    public static void main(String[] args) {
        System.out.println(reverseWord("Hi".toUpperCase()));
        reverseWord();
        reverseNumber(336);
    }

    public static int reverseNumber(int number) {
        int revNumber = 0;
        while (number >0) {
            revNumber = number % 10;
            System.out.print(revNumber);
            number /= 10;
        }
        return revNumber;
    }
}
