package section_5.xcodingchallenge;

public class Palindrome {
    public static boolean isPalindrome(int number){
        int originalNumber = number;
        int reverseNumber = 0;

        do {
            int lastDigit = number % 10;
            reverseNumber = (reverseNumber * 10) + lastDigit;
            number /= 10;
        } while (number != 0);
        if (reverseNumber == originalNumber) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        System.out.println(isPalindrome(212));
    }
}
