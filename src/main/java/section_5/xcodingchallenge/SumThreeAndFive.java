package section_5.xcodingchallenge;

public class SumThreeAndFive {
    public static void main(String[] args) {
        int count = 0;
        int sum = 0;
        for (int number = 1; number <= 100; number++) {
            if ((number % 3 == 0) && (number % 5 == 0)) {
                count++;
                sum += number;
                System.out.println(number);
            }
            if (count == 5) {
                break;
            }
        }
        System.out.println("Sum is " + sum);
    }

}
