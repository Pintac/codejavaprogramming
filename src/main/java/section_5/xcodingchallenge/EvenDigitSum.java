package section_5.xcodingchallenge;

public class EvenDigitSum {
    public static int getEvenDigitSum(int number) {
        int sum = 0;
        int evenDigits;
        if (number < 0) {
            sum = -1;
        } else {
            do{
                evenDigits = number%10;
                if(evenDigits%2==0){
                    sum+=evenDigits;
                }
                number/=10;
            }while (number>0);
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(getEvenDigitSum(123456789));
    }
}
