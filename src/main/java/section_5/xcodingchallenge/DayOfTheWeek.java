package section_5.xcodingchallenge;

public class DayOfTheWeek {
    private static void printDayOfTheWeekUsingIf(int day) {
        String theDay;
        if (day < 0) {
            theDay = "Invalid value";
        } else if (day == 0) {
            theDay = "Sunday";
        } else if (day == 1) {
            theDay = "Monday";
        } else if (day == 2) {
            theDay = "Tuesday";
        } else if (day == 3) {
            theDay = "Wednesday";
        } else if (day == 4) {
            theDay = "Thursday";
        } else if (day == 5) {
            theDay = "Friday";
        } else if (day == 6) {
            theDay = "Saturday";
        } else {
            theDay = "None of the days";
        }
        System.out.println(theDay);
    }

    private static void printDaysOfWeekUsingSwitch(int day) {
        switch (day) {

            case 0:
                System.out.println("Day is Sunday");
                break;
            case 1:
                System.out.println("Day is Monday");
                break;
            case 2:
                System.out.println("Day is Tuesday");
                break;
            case 3:
                System.out.println("Day is Wednesday");
                break;
            case 4:
                System.out.println("Day is Thursday");
                break;
            case 5:
                System.out.println("Day is Friday");
                break;
            case 6:
                System.out.println("Day is Saturday");
                break;
            default:
                System.out.println("None of the days");
                break;
        }
    }

    public static void main(String[] args) {
        printDayOfTheWeekUsingIf(7);
        printDayOfTheWeekUsingIf(5);
        printDaysOfWeekUsingSwitch(1);
        printDaysOfWeekUsingSwitch(2);
        printDaysOfWeekUsingSwitch(3);
        printDaysOfWeekUsingSwitch(4);
        printDaysOfWeekUsingSwitch(5);
        printDaysOfWeekUsingSwitch(6);
        printDaysOfWeekUsingSwitch(0);
    }
}
