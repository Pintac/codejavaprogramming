package section_5.loopstatements;

public class DoWhileLoop {
    public static void main(String[] args) {
        int count = 4;
        int evenFound = 0;
        int totalEvenFound = 0;
        do {
            count++;
            if(!isEvenNumber(count)){
                continue;
            }else{
                evenFound++;
                totalEvenFound+=count;
                System.out.println("Even number "+count);
                if(evenFound==5){
                    break;
                }
            }

        } while (count != 20);
        System.out.println("********************************");
        System.out.println("Total of even numbers found "+evenFound);
        System.out.println("Sum of all even numbers found "+totalEvenFound);
    }

    public static boolean isEvenNumber(int number) {
        if(number%2==0){
            return true;
        }
        else {return false;}
    }
}
