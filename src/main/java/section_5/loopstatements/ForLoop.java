package section_5.loopstatements;

public class ForLoop {
    public static void main(String[] args) {
        String.format("%.2f", calculateInterest(10000, 2));
    }

    public static double calculateInterest(double amount, double interestRate) {
        double total = 0;

        for (double counter = interestRate; counter <= 8; counter++) {
            total = (amount * counter / 100);
            System.out.println(amount + " at " + counter + "% interest is " + total);
        }
        return total;
    }
}
