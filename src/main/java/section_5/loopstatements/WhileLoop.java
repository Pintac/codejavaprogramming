package section_5.loopstatements;

public class WhileLoop {
    public static void main(String[] args) {
        int x = 6;
        while (x>5) {
            System.out.println(x);
            for(int y =0; y<=5;y++){
                System.out.println("\t"+y);
            }
            x++;
        }

    }
}
