package section_5.passingvalues;

import java.util.Scanner;

public class ScannerClass {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter your year of birth");

        boolean hasNextInt = scanner.hasNextInt();

        if (hasNextInt) {
            int yearOfBirth = scanner.nextInt();
            scanner.nextLine();

            System.out.println("Enter your name: ");
            String name = scanner.nextLine();
            int age = 2020 - yearOfBirth;

            if (age >= 0 && age <= 100) {
                System.out.println("Hi " + name + ", you are " + age + " years old");
            } else {
                System.out.println("Wrong input");
            }
        } else {
            System.out.println("Unable to pass year of birth");
        }
        scanner.close();
    }
}
