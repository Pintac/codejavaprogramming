package section_5.passingvalues;

public class Conversion {
    public static void main(String[] args) {
        String numberAsString = "2020.00";
        System.out.println("numberAsString "+numberAsString);

        double number = Double.parseDouble(numberAsString);
        System.out.println("number "+number);

        numberAsString +=3;
        number +=3;

        System.out.println(numberAsString);
        System.out.println(number);

    }
}
