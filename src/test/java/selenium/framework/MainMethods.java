package selenium.framework;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MainMethods {
    private WebDriver driver;

    public void setDriver() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    public void get(String URL) {
        try {
            driver.get(URL);
        } catch (Exception e) {
            System.out.println(URL+ " is an invalid link");
        }
    }

    public void maximizeScreen() {
        driver.manage().window().maximize();
    }

    public void click(By by) {
        try {
            driver.findElement(by).click();
        } catch (Exception e) {
            System.out.println("Can not find \""+by+ "\" element");
        }
    }

    public void closeBrowser() {
        driver.close();
    }

    public void sendKeys(By by, String text) {
        try {
            driver.findElement(by).sendKeys(text);
        } catch (Exception e) {
            System.out.println("Can not find \""+by+ "\" element");
        }
    }
}
