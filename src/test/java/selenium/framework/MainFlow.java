package selenium.framework;

import org.openqa.selenium.By;

public class MainFlow {
    MainMethods mainMethods = new MainMethods();

    public By SEARCHICON = By.id("sb_form_q");
    public By SIGNIN = By.xpath("//*[@id=\"sb_form\"]/label/svg");
    public By AMAZONSEARCH = By.id("twotabsearchtextbox");
    public By SEARCHBAR = By.xpath("//*[@id=\"nav-search-submit-text\"]/input");
    public String item = "IAMS for cats";

    public void testMainFlow(){
        mainMethods.sendKeys(SEARCHICON, "Python Programming");
        
    }
    public void getURL(String URL) {
        mainMethods.setDriver();
        mainMethods.get(URL);
        mainMethods.maximizeScreen();
        enterText(AMAZONSEARCH,item);
        mainMethods.click(SEARCHBAR);
    }
    public void enterText(By by, String URL) {
       mainMethods.sendKeys(by,URL);
    }

    public void enterSearchWord(String URL) {
        System.out.println(URL + mainMethods.getClass());
       // mainMethods.closeBrowser();
    }
}






