package selenium.seleniumtests;

import org.testng.annotations.Test;
import selenium.framework.MainFlow;

public class Demo {

    MainFlow flow = new MainFlow();

    @Test(priority = 0)
    public void testingChromeBrowser() throws InterruptedException {

        flow.getURL("https://www.bing.com/");
        flow.testMainFlow();
    }

    @Test(priority = 1)
    public void testingIEBrowser() throws InterruptedException {

    }


}
